<div class="container">
<div class="row ">
    <div class="col-md-3">    
    <a id= "btnAgregar" href="../Controlador_digitador/cargarFormularioReserva">
    <button  type="button" class="btn btn-info"> <i class="glyphicon glyphicon-plus"></i> 
              Digitar nueva Reserva
    </button> 
     </a>
 </div>
  </div>

<br>



<div class="container" style="">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Lista de Reservas</h3> 
                   </div>
    <!-- tabla que muestra los datos del vehiculo-->
        <table id ="tabla_dinamica" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" >
            <thead> 
              <tr>
                        <th>ID</th>
                        <th>Conductor</th>
                        <th>Rut Conductor</th>
                        <!--<th>Rut Digitador</th>-->
                        <th>Patente Vehiculo</th>
                        <!--<th>Km salida</th>--> <!-- nota: No es necesario mostrar el estado en la tabla-->
                       <!-- <th>Km LLegada</th>
                        <th>Km Recorridos</th>-->
                        <th>Fecha</th>
                        <th>Destino</th>
                        <th>Horario</th>
                        <!--<th>Carga de Combustible</th>
                        <th>Observaciones</th>-->
                        <th>Opciones</th>

                       
                    </tr>
            </thead> 
            
            <tbody> 
                 <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                       <tr>
                                                          
                           <td> <?=$row->id_agenda?></td> 
                           <td> <?=$row->nombre_conductor?> </td>
                           <td> <?=$row->rut_conductor?> </td>
                           <!--<td> <?=$row->rut_usuario?> </td>-->
                           <td> <?= $row->vehiculo_patente?> </td>
                           <!--<td> <?= $row->km_salida?> </td>
                           <td> <?= $row->km_llegada?> </td>
                           <td> <?= $row->km_Recorridos?> </td>-->
                           <td> <?= $row->fecha?> </td>
                           <td> <?= $row->destino?> </td>
                           <td> <?= $row->horario?> </td>
                           <!--<td> <?= $row->carga_Combustible?> </td>
                           <td> <?= $row->observaciones?> </td>-->

                        
                            <td class="col-lg-4"> 

                             <button tittle="<?=$row->id_agenda?>" type="button" class="ver_detalle_reservas btn btn-primary" data-toggle="modal" data-target="#detalleReservas">
                                        <i class="glyphicon glyphicon-search"></i> 
                                  Detalle
                                </button>


                              <a href="cargarFormularioReserva_editar/<?=$row->id_agenda?>" class=""> 
                              <button   type="button" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> 
                               Editar
                              </button>
                              </a>
                              
                            </td>
                       </tr> 
                  <?php endforeach;?>                    
            </tbody>
       </table>    
      </div>
   </div>
</div>
</div>

<!--********************************* MODAL DETALLE RESERVAS ***************************************-->
<div class="modal fade" id="detalleReservas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Detalle Reserva</h4>
        </div>
          <div class="modal-body">

           <form class="form-inline" role="form" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
         <div class="container">
            <div class="row">          
            <br>
            <div class="col-md-4">
              <div class="container">
            <div class="row">          
              <div class="col-md-6">
               <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" >

              <tr>
                  <td><label for="focusedInput" >ID agenda: </label></td> 
                  <td><input id="id_agenda" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Rut conductor: </label></td>
                  <td><input id="rut_conductor" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Nombre conductor: </label></td>
                  <td><input id="nombre_conductor" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Vehículo: </label></td>
                  <td><input id="vehiculo_patente" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Fecha: </label></td>
                  <td><input id="fecha" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Horario</label></td>
                  <td><input id="horario" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Rut Digitador: </label></td>
                  <td><input id="rut_usuario" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              <tr>
                  <td><label for="focusedInput" >Destino: </label></td>
                  <td><input id="destino" name=""type="text" style="border:0" readonly></input></td>
              </tr>
              
              <tr>
                  <td><label for="focusedInput" >Observaciones: </label></td>
                  <td><textarea id="observaciones"  rows="2" cols="50" name=""type="text" style="border:0" readonly></textarea></td>
               </tr>
             </table>

              <!--Estado??-->
              </div>
            </div>
            </div>
          </div>
        </div>
       </form>
     </div>
    </div>
  </div>
</div>


<script type="text/javascript">
   $(document).ready(function (){
       $(".ver_detalle_reservas").click(function(e){
        //e.preventDefault();                         //Evita que el evento se genere (redireccionar)
        
        var id= $(this).attr("tittle");                 //Se obtiene el id de la bitácora
        var url= "<?php echo base_url().'index.php/digitador/Controlador_digitador/detalleReserva/'?>"+id;
        $.ajax({
                  url: url,
                  type: "POST",
                  //data : data,
                  dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                         $("#id_agenda").val(data.id_agenda);
                         $("#rut_conductor").val(data.rut_conductor);
                         $("#nombre_conductor").val(data.nombre_conductor); 
                         $("#vehiculo_patente").val(data.vehiculo_patente);
                         $("#fecha").val(data.fecha);
                         $("#horario").val(data.horario);
                         $("#rut_usuario").val(data.rut_usuario);
                         $("#destino").val(data.destino);
                         $("#observaciones").val(data.observaciones);

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
     });
 });
</script>

<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#tabla_dinamica').dataTable();

          });
</script> 



<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#tabla_dinamica').dataTable();
            $("div#tabla_dinamica_length").empty();
            $("div#tabla_dinamica_paginate").empty();
            $("div#tabla_dinamica_info").empty();
            $("div#tabla_dinamica_wrapper").removeClass();
            $("div#tabla_dinamica_wrapper").addClass("form-group");
            $("div#tabla_dinamica_filter").find("input").addClass("form-control");
            $("div#tabla_dinamica_filter").removeClass();
            $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
            //$("div#tabla_dinamica_filter").find("label").remove();
           // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
            $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
            $(".dataTables_paginate").empty();
            $(".paging_simple_numbers").empty();
             
          });
</script> 




