
<br>
<br>


<div class="container" style="">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Lista de Reservas</h3> 
                   </div>
           <!-- tabla que muestra los datos del vehiculo-->
              <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="example"  >
                  <thead> 
                    <tr>
                              <th>ID</th>
                              <th>Conductor</th>
                              <th>Rut Conductor</th>
                              <!--<th>Rut Digitador</th>-->
                              <th>Patente Vehiculo</th>
                          <!-- <th>Km LLegada</th>
                              <th>Km Recorridos</th>-->
                              <th>Fecha</th>
                              <th>Destino</th>
                              <th>Horario</th>
                              <!--<th>Carga de Combustible</th>
                              <th>Observaciones</th>-->
                              <th>Opciones</th>

                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                                                
                                 <td> <?=$row->id_agenda?></td> 
                                 <td> <?=$row->nombre_conductor?> </td>
                                 <td> <?=$row->rut_conductor?> </td>
                                 <!--<td> <?=$row->rut_usuario?> </td>-->
                                 <td> <?= $row->vehiculo_patente?> </td>
                                 <!--<td> <?= $row->km_salida?> </td>
                                 <td> <?= $row->km_llegada?> </td>
                                 <td> <?= $row->km_Recorridos?> </td>-->
                                 <td> <?= $row->fecha?> </td>
                                 <td> <?= $row->destino?> </td>
                                 <td> <?= $row->horario?> </td>
                                 <!--<td> <?= $row->carga_Combustible?> </td>
                                 <td> <?= $row->observaciones?> </td>-->

                              
                                  <td class="col-lg-4"> 

                                 
                                    <a href="<?php echo base_url();?>index.php/digitador/Controlador_digitador2/cargarFormularioReserva_completar/<?=$row->id_agenda?>" class="">
                                      <button  type="button" class="btn btn-primary">
                                              <i class="glyphicon glyphicon-edit"></i> 
                                        Completar Reserva de vehículo
                                      </button>
                                    </a>

                                  
                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
      
      </div>
   </div>
</div>
</div>