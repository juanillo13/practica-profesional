
       <!-- CONSTRUCCION DEL FORMULARIO--> 
<div class="container">

</div>
<br>
  <div class="container">
        <div class="row">
                <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Completar Reserva de Vehículo</h3>
                </div>
 <!-- esta es la forma de pasar los datos mediante un formulario -->
  <form class="form-inline" role="form" action="../completarReserva" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
   <div class="container">
      <div class="row">
   
   <?php foreach($resultado as $row):?>
  <br>
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" > ID agenda: </label>
    <input class="form-control" id="id_agenda" name ="id_agenda" type="text"  value="<?=$row->id_agenda?>"  readonly  >
    </div>
  </div>

  <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Conductor: </label>
    <!--<input class="form-control" id="nombre_conductor" name="nombre_conductor" type="text" value="<?=$row->nombre_conductor?>">
     -->
    <select class="form-control" id="nombre_conductor" style=" border-width:2px;" name="nombre_conductor">
    <!--rellenado con jquery-->                   
    </select>
    </div>
    </div>
   



    


  <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Rut Conductor: </label>
    <select class="form-control" id="rut_conductor" style=" border-width:2px;" name="rut_conductor">
                      
    </select>    
   </div>
    </div>

    <br>
    <br>
    <br>

     <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Vehículo: </label>
    <select class="form-control" id="patente_vehiculo" style=" border-width:2px;" name="patente_vehiculo">
    <!--rellenado con jquery-->                   
    </select>    </div>
    </div>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Fecha: </label>
    <input class="form-control" id="datepicker" name="fecha" type="text"  value="<?=$row->fecha?>"  readonly >
    </div>
    </div>

    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Horario: </label>
    <input class="form-control" id="horario" name="horario" value="<?=$row->horario?>"  readonly>
                
               
     </input>
    </div>
    </div><!--3ero-->

    <br>
    <br>
    <br>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Destino: </label>
    <input class="form-control" id="destino" name="destino" type="text" value="<?=$row->destino?>" readonly>
    </div>
    </div>

   
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Estado: </label>
    <input class="form-control" id="estado" name="estado"  value="<?=$row->estado?>" readonly>
                
                
              
     </input>
    </div>
    </div>
    
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Rut Digitador: </label>
    <input class="form-control" id="rutdigitador" name ="rutdigitador" type="text" value="<?=$digitador?>" readonly >
    </div>
    </div>



    <br>
    <br>
    <br>

    <div class="col-xs-10">
    <div class="form-group ">

    <label for="focusedInput" >Observaciones: </label>
    
    <textarea  class="form-control"  id="observaciones" name="observaciones" cols="150" rows="5" placeholder="Las observaciones son obligatorias"><?=$row->observaciones?></textarea>
    </div>
    </div>

    <?php endforeach;?> 
   
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
               
   <div class="container">
    <div class="fluid-row">

  <div class="form-group">
  <div class="col-xs-offset-7 col-md-10">
       <button id="botonAgregar" type="button"  style ="width:450px;" class="btn btn-success btn-lg">Completar</button>
  </div>
  </div>


  <br><br>

 
 </form>



  </div>

   </div>




                        </div>



</div>
</div>


</div>
</div>
</div>
</div>


    

<!--CAMBIO DE ESTILOS EN CSS PARA INPUTS-->
<style type="text/css">
div {
   margin: .4em 0,0,0;
}
div label {
  width: 50%;
  float: left;

}
</style>



<script type="text/javascript">
       $(document).ready(function (){               
         var url= "<?php echo base_url().'index.php/Principal/getConductores'?>";
       
         $.ajax({                  
                url: url,
                type: "POST",
                dataType: "JSON",
 
                success: function(data)  {
                //Limpiamos el select
                $('#nombre_conductor').empty();
                //Recorremos los vectores, generamos y agregamos los option
                //con los datos de los profesores y empresas respectivamente
                //Agregamos opciones de los nombres de conductores que existen
                $('#nombre_conductor').append('<option value="Sin información">Sin información</option>');
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {   
                        $('#nombre_conductor').append('<option value='+i+'>'+data[i].Nombre+'</option>');
                     }          
                }
               //Agregamos opciones de los rut de conductores que existen
                $('#rut_conductor').append('<option value="Sin información">Sin información</option>');
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {  
                        $('#rut_conductor').append('<option value='+i+'>'+data[i].Rut+'</option>');
                     } 
                }
                //Agregamos opciones de los rut de conductores que existen
                $('#patente_vehiculo').append('<option value="Sin información">Sin información</option>');
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="vehiculo") {  
                        $('#patente_vehiculo').append('<option value='+data[i].Patente+'>'+data[i].Patente+'</option>');
                     } 
                }
 
                },
                error: function(result) {
                console.log("Error: " + result.statusText);
                }

          });
    });       
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#rut_conductor").click(function(e){
        var option = $('option:selected', this).attr('value');
        $("#nombre_conductor").val(option);       
    
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#nombre_conductor").click(function(e){
       
        var option = $('option:selected', this).attr('value');
        $("#rut_conductor").val(option);
       
    
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#botonAgregar").click(function(e){

        e.preventDefault(); 
        var url= "<?php echo base_url().'index.php/digitador/Controlador_digitador2/completarReserva'?>";
   


        var data = $(this).serializeArray();
        data.push({name:'nombre_conductor',value: $('option:selected',$("#nombre_conductor")).text()});
        data.push({name:'rut_conductor',value: $('option:selected',$("#rut_conductor")).text()});
        data.push({name:'rut_usuario',value: $("#rutdigitador").val()});
        data.push({name:'vehiculo_patente',value: $("#patente_vehiculo").val()});
        data.push({name:'fecha',value: $("#datepicker").val()});
        data.push({name:'destino',value: $("#destino").val()});
        data.push({name:'horario',value: $("#horario").val()});
        data.push({name:'observaciones',value: $("#observaciones").val()});
        data.push({name:'id_agenda',value: $("#id_agenda").val()});

        $.ajax({
                  url: url,
                  type: "POST",
                  data : data,
                  //dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                   // href.location("digitador/Controlador_digitador2");
                    location.href = '../';

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
    
     });
 });
</script>




