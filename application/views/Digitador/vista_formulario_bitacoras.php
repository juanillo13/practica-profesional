<!-- CONSTRUCCION DEL FORMULARIO--> 
<br>
  <div class="container">
        <div class="row">
                <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Agregar Bitácora</h3>
                </div>
 <!-- esta es la forma de pasar los datos mediante un formulario -->
  <form class="form-inline" role="form" action="../Controlador_digitador/agregarBitacora" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
   <div class="container">
      <div class="row">
    
    <br>
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" > N° bitácora: </label>
    <input class="form-control n_bitacora" id="n_bitacora" name ="n_bitacora" type="number" style=" border-width:2px;" required>
    </div>
  </div>

  <div class="col-md-4">
          <div class="form-group">
            <label for="focusedInput" >Nombre Conductor: </label>
            <select class="form-control" id="nombre_conductor" style=" border-width:2px;" name="nombre_conductor">
            <!--rellenado con jquery-->                   
            </select>
          </div>
  </div>

   <div class="col-md-4">
          <div class="form-group">
            <label for="focusedInput" >Rut Conductor: </label>
            <select class="form-control" id="rut_conductor" style=" border-width:2px;" name="rut_conductor">
                <!--rellenado con jquery-->               
            </select>    
           </div>
        </div>


    <br>
    <br>
    <br>

     <div class="col-md-4">
        <div class="form-group">
          <label for="focusedInput" >Vehículo: </label>
          <select class="form-control" id="patente_vehiculo" style=" border-width:2px;" name="patente_vehiculo">
          <!--rellenado con jquery-->                   
          </select>    
      </div>
    </div>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Fecha: </label>
    <input class="form-control" id="datepicker" name="fecha" type="text" style=" border-width:2px;" required>
    </div>
    </div>

    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Destino</label>
    <input class="form-control" id="destino" name="destino" type="text" style=" border-width:2px;" required>
    </div>
    </div>

   

    <br>
    <br>
    <br>
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >KM salida: </label>
    <input class="form-control" id="km_salida" name="km_salida" type="number" style=" border-width:2px;" required>
    </div>
    </div>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >KM llegada: </label>
    <input class="form-control" id="km_llegada" name="km_llegada" type="number" style=" border-width:2px;" required>
    </div>
    </div>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >KM recorridos: </label>
    <input class="form-control" id="km_Recorridos" name="km_Recorridos" type="number" style=" border-width:2px;" required>
    </div>
    </div>


     <br>
    <br>
    <br>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Carga de combustible: </label>
    <input class="form-control" id="combustible"  name="combustible" type="number"  placeholder="Si no carga se ingresa 0" style=" border-width:2px;" required>
    </div>
    </div>





    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Estado: </label>
    <select class="form-control" id="estado" style=" border-width:2px;" name="estado">
                
                <option value="0">activo</option>
                <option value="1">inactivo</option>
              
     </select>
    </div>
    </div>

    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Rut Digitador: </label>
    <input class="form-control" id="rut_digitador" name ="rut_digitador" style=" border-width:2px;" type="text" value="<?=$rut?>" readonly>
    </div>
    </div>


    <br>
    <br>
    <br>

    <div class="col-xs-10">
    <div class="form-group ">

    <label for="focusedInput" >Observaciones: </label>
    
    <textarea  class="form-control"  id="observaciones" name="observaciones" cols="150" rows="5" placeholder="Las observaciones son obligatorias" required></textarea>
    </div>
    </div>


    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
     <br>
    

         <div class="container">
          <div class="fluid-row">

        <div align="central">
        <div class="col-xs-offset-3 col-md-10">
             <button id="botonAgregar"type="button"  style ="width:450px;" class="btn btn-success btn-lg">Agregar </button>
        </div>
        </div>
      



       
       
         </div>
                        </div>


<br>
<br>

  


</div>
</div>
  </form>

                       

</div>
</div>
</div>
</div>

                     


  <!--CARGA PLUGINS DATEPICKER PARA DESPLEGAR CALENDARIO PARA FECHAS-->
<script>
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
$("#datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
});
</script>

    <!--CAMBIO DE ESTILOS EN CSS PARA INPUTS-->
    <style type="text/css">
    div {
       margin: .4em 0,0,0;
    }
    div label {
      width: 50%;
      float: left;

    }


    </style>

<script type="text/javascript">
   $(document).ready(function (){
       $("#n_bitacora").focusout(function(e){
       
        var texto= $(this).val();                 //Se obtiene el id de la bitácora
        var url= "<?php echo base_url().'index.php/Principal/validarPK'?>";
   
        var data = $(this).serializeArray();
        data.push({name:'texto',value:texto});
        data.push({name:'columna',value:"n_bitacora"});
        data.push({name:'tabla',value:"bitacora"});
        
        $.ajax({
                  url: url,
                  type: "POST",
                  data : data,
                  dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                  if(data.resultado=="existe") {
                      
                      $.alert({
                        title: 'Error!',
                        content: 'La clave primaria ya existe',
                        confirmButton:'Aceptar',
                        icon: 'fa fa-warning',
                        confirmButtonClass: 'btn-danger',
                        theme: 'white',       //Puede ser white (normal)
                        //confirmKeys: [13], // ENTER key
                       // cancelKeys: [27], // ESC key
                        confirm: function(){
                             $("#n_bitacora").val("");
                          }                                               
                        });                      
                        
                    }

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#rut_conductor").click(function(e){
        var option = $('option:selected', this).attr('value');
        $("#nombre_conductor").val(option);       
    
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#nombre_conductor").click(function(e){
       
        var option = $('option:selected', this).attr('value');
        $("#rut_conductor").val(option);
    
     });
 });
</script>



<script type="text/javascript">
       $(document).ready(function (){               
         var url= "<?php echo base_url().'index.php/Principal/getConductores'?>";
       
         $.ajax({                  
                url: url,
                type: "POST",
                dataType: "JSON",
 
                success: function(data)  {
                //Limpiamos el select
                $('#nombre_conductor').empty();
                //Recorremos los vectores, generamos y agregamos los option
                //con los datos de los profesores y empresas respectivamente
                //Agregamos opciones de los nombres de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {   
                        $('#nombre_conductor').append('<option value='+i+'>'+data[i].Nombre+'</option>');
                     }          
                }
               //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {  
                        $('#rut_conductor').append('<option value='+i+'>'+data[i].Rut+'</option>');
                     } 
                }
                //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="vehiculo") {  
                        $('#patente_vehiculo').append('<option value='+data[i].Patente+'>'+data[i].Patente+'</option>');
                     } 
                }
 
                },
                error: function(result) {
                console.log("Error: " + result.statusText);
                }

          });
    });       
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#botonAgregar").click(function(e){

        e.preventDefault(); 
        var url= "<?php echo base_url().'index.php/digitador/Controlador_digitador/agregarBitacora'?>";
   
        var data = $(this).serializeArray();
        data.push({name:'n_bitacora',value: $("#n_bitacora").val()});
        data.push({name:'nombre_conductor',value: $('option:selected',$("#nombre_conductor")).text()});
        data.push({name:'rut_conductor',value: $('option:selected',$("#rut_conductor")).text()});
        data.push({name:'rut_usuario',value: $("#rut_digitador").val()});
        data.push({name:'vehiculo_patente',value: $("#patente_vehiculo").val()});
        data.push({name:'fecha',value: $("#datepicker").val()});
        data.push({name:'destino',value: $("#destino").val()});
        data.push({name:'estado',value:$("#estado").val()});
        data.push({name:'carga_Combustible',value:$("#combustible").val()});
        data.push({name:'km_salida',value:$("#km_salida").val()});
        data.push({name:'km_Recorridos',value:$("#km_Recorridos").val()});
        data.push({name:'km_llegada',value:$("#km_llegada").val()});
        data.push({name:'observaciones',value:$("#observaciones").val()});

        $.ajax({
                  url: url,
                  type: "POST",
                  data : data,
                  //dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                   // href.location("digitador/Controlador_digitador2");
                    location.href = '../Controlador_digitador/verBitacoras';

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
    
     });
 });
</script>

