
                    
<div class="container">
<div class="row ">
    <div class="col-md-3">    
    <a id= "btnAgregar" href="../Controlador_digitador/cargarFormularioBitacora">
    <button  type="button" class="btn btn-info"> <i class="glyphicon glyphicon-plus"></i> 
              Digitar nueva Bitácora
    </button> 
     </a>
 </div>
  </div>
 <div>
 <br>
 <br>
<div class="container" style="">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Lista de Bitácoras</h3> 
                 </div>
                   
      <!-- tabla que muestra los datos del vehiculo-->
              <table id="tabla_dinamica"  cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover"  >
                  <thead> 
                    <tr>
                              <th>N° Bitácora</th>
                              <th>Conductor</th>
                              <th>Rut Conductor</th>
                              <th>Patente Vehiculo</th>
                              <th>Fecha</th>
                              <th>Destino</th>
                              <th >Opciones</th>
                              <th>Subir archivos</th>
                              <th>Descarga archivos</th>

                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row): ?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                                                
                                 <td> <?=$row->n_bitacora?> </td> 
                                 <td> <?=$row->nombre_conductor?> </td>
                                 <td> <?=$row->rut_conductor?> </td>
                                 <!--<td> <?=$row->rut_usuario?> </td>-->
                                 <td> <?= $row->vehiculo_patente?> </td>
                                 <!--<td> <?= $row->km_salida?> </td>
                                 <td> <?= $row->km_llegada?> </td>
                                 <td> <?= $row->km_Recorridos?> </td>-->
                                 <td> <?= $row->fecha?> </td>
                                 <td> <?= $row->destino?> </td>
                                 <!--<td> <?= $row->carga_Combustible?> </td>
                                 <td> <?= $row->observaciones?> </td>-->

                              
                                  <td class="col-lg-2"> 
                              
                                   <div class="form-group">
                                      <button type="button" tittle="<?=$row->n_bitacora?>" class=" btn-sm ver_detalle btn btn-primary" data-toggle="modal" data-target="#detalleBitacora">
                                              <i class="glyphicon glyphicon-search"></i> 
                                        Detalle
                                      </button>
                                    

                                    <a href="cargarFormularioBitacora_editar/<?=$row->n_bitacora?>" class=""> 
                                    <button   type="button" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-edit"></i> 
                                     Editar
                                    </button>
                                    </a>

                           
                                    </div>
                                   
                                   
                                 </td>
                                 <td class="col-lg-2">
                                   <!-- the avatar markup -->
                                

                                     <?php
                                    if ($row->archivo ==NULL) {?>
                                    <!--En caso de que  en la base de datos no hayan subido archivos-->
                                        
                                    <div id="kv-avatar-errors" class="subirajax" style="width:800px;display:none"></div>
                                    <form id="<?=$row->n_bitacora?>" class="text-center"  type ="<?=$row->n_bitacora?>" action="/avatar_upload.php" method="post" enctype="multipart/form-data">
                                        <div class="kv-avatar center-block" style="width:230px">
                                            <input id="<?=$row->n_bitacora?>" name="<?=$row->n_bitacora?>" type="file" class="avatar file-loading">
                                        </div>
                                        <!-- include other inputs if needed and include a form submit (save) button -->
                                    </form>
                                    </div>
                                    <?php } 
                                     else { ?>
                                        
                                    <div id="kv-avatar-errors" class="subirajax" style="width:800px;display:none"></div>
                                    <form id="<?=$row->n_bitacora?>" class="text-center"  type ="<?=$row->n_bitacora?>" action=""  method="post" enctype="multipart/form-data" data-toggle="tooltip" data-placement="top" title="Puede editar el archivo ya subido">
                                        <div class="kv-avatar center-block" style="width:230px">
                                            <input id="<?=$row->n_bitacora?>" name="<?=$row->n_bitacora?>" type="file" class="avatar file-loading">
                                        </div>
                                        <!-- include other inputs if needed and include a form submit (save) button -->
                                    </form>
                                    </div> 
                                      <?php  }?>    

                                     
                                </td>
                                <td>
                                   
                                    <!--OPCION PARA DESCARGAR EL ARCHIVO-->
                                    <?php
                                    if ($row->archivo ==NULL) {?>
                                    <!--En caso de que  en la base de datos no hayan subido archivos-->
                                        <a href="" data-toggle="tooltip" data-placement="top" title="No se ha subido el archivo!" class="download-<?=$row->n_bitacora?>"> 
                                        <button   type="button" class="btn btn-sm btn-danger" disabled><i class="glyphicon glyphicon-download"></i> 
                                         Descargar
                                        </button>
                                        </a>
                                    <?php } 
                                    else { ?>
                                         <a href="<?php echo base_url().'index.php/digitador/Controlador_digitador/descargarArchivo/'.$row->n_bitacora?>" class="download-<?=$row->n_bitacora?>" type=""> 
                                        <button   type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-download"></i> 
                                         Descargar
                                        </button>
                                        </a> 
                                       

                                      <?php }?>                            

                                </td> 
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
      
      </div>
   </div>  

</div>



<!--********************************* MODAL DETALLE BITACORA ***************************************-->
<div class="modal fade" id="detalleBitacora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Detalle bitácora</h4>
          </div>
          <div class="modal-body">

           <form class="form-inline" role="form" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
               <div class="container">
                  <div class="row">          
                  <div class="col-md-6">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">

                          <tr>
                            <td><label for="focusedInput" >N° bitácora: </label></td>
                            <td> <input id="n_bitacora" name=""type="text" style="border:0" outline="none" readonly></input></td>
                          </tr>
                          <tr>
                            <td><label for="focusedInput" >Nombre conductor: </label></td>
                            <td><input id="nombre_conductor" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >Conductor: </label></td>
                            <td><input id="rut_conductor" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >Vehículo: </label></td>
                            <td><input id="vehiculo" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >Fecha: </label></td>
                            <td><input id="fecha" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >Destino</label></td>
                            <td><input id="destino" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >KM salida: </label></td>
                            <td><input id="km_salida" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >KM llegada: </label></td>
                            <td><input id="km_llegada" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >KM recorridos: </label></td>
                            <td><input id="km_recorridos" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                          <tr>
                            <td><label for="focusedInput" >Carga de combustible: </label></td>
                            <td><input id="carga_combustible" name=""type="text" style="border:0" readonly></input></td>
                           </tr>
                    
                            <tr>
                              <td><label for="focusedInput" >Rut Digitador: </label></td>
                              <td><input id="rut_digitador" name=""type="text" style="border:0" readonly></input></td>
                             </tr>
                          <tr>
                            <td><label for="focusedInput" >Observaciones: </label></td>
                            <td><textarea id="observaciones" rows="5" cols="10" name=""type="text" style="border:0" readonly></textarea></td>
                         </tr><br>
                    </table>
                 </div>
             </div>
           </div>
       </form>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
   $(document).ready(function (){
       $(".ver_detalle").click(function(e){
        //e.preventDefault();                         //Evita que el evento se genere (redireccionar)
        
        var id= $(this).attr("tittle");                 //Se obtiene el id de la bitácora
        var url= "<?php echo base_url().'index.php/digitador/Controlador_digitador/detalleBitacora/'?>"+id;
        $.ajax({
                  url: url,
                  type: "POST",
                  //data : data,
                  dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                         $("#n_bitacora").val(data.n_bitacora);
                         $("#nombre_conductor").val(data.nombre_conductor);
                         $("#rut_conductor").val(data.rut_conductor); 
                         $("#vehiculo").val(data.vehiculo);
                         $("#fecha").val(data.fecha);
                         $("#destino").val(data.destino);
                         $("#km_salida").val(data.km_salida);
                         $("#km_llegada").val(data.km_llegada);
                         $("#km_recorridos").val(data.km_recorridos);
                         $("#carga_combustible").val(data.carga_combustible);
                         $("#rut_digitador").val(data.rut_digitador);
                         $("#observaciones").val(data.observaciones);

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
     });
 });
</script>

<script type="text/javascript" charset="utf-8"> 
 $(document).ready(function() {
            $('#tabla_dinamica').DataTable({
                  "language": {
                      "lengthMenu": "Display _MENU_ records per page",
                      "zeroRecords": "No se encontró ninguna entrada",
                      "info": "Mostrando _PAGE_ de _PAGES_",
                      "infoEmpty": "No hay registros disponibles",
                      "infoFiltered": "(Filtrado de los registros totales _MAX_ )"
                   }
              });
              $("div#tabla_dinamica_length").empty();
              $("div#tabla_dinamica_paginate").empty();
              $("div#tabla_dinamica_info").empty();
              $("div#tabla_dinamica_wrapper").removeClass();
              $("div#tabla_dinamica_wrapper").addClass("form-group");
              $("div#tabla_dinamica_filter").find("input").addClass("form-control");
              $("div#tabla_dinamica_filter").removeClass();
              $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
              //$("div#tabla_dinamica_filter").find("label").remove();
             // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
              $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
              $(".dataTables_paginate").empty();
              $(".paging_simple_numbers").empty();
             
          });
</script> 


<!-- some CSS styling changes and overrides -->
<style type="text/css">
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}

</style>


<!--
<style type="text/css">
#tabladinamica tr {
    text-align: center;
}
</style>-->

<!-- your server code `avatar_upload.php` will receive `$_FILES['avatar']` on form submission -->
 
<!-- the fileinput plugin initialization -->
<script type="text/javascript">
  var btnCust = '<button type="button" class="btn btn-default" title="Add picture tags" ' + 
      'onclick="alert(\'Call your custom code here.\')">' +
      '<i class="glyphicon glyphicon-tag"></i>' +
      '</button>'; 
  $(".avatar").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      browseLabel: '',
      removeLabel: '',
      browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
      removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt="Your Avatar" style="width:160px">',
      layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif"]
});
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $( "form" ).submit(function( e ) {
        e.preventDefault();   
        var url= "<?php echo base_url().'index.php/digitador/Controlador_digitador/subirArchivo/'?>";
        var id="";
        var formulario= $(this);
        var formData = new FormData();
        var datos = formulario.serialize();
        var data = $(this).serializeArray();
        

        //formData.append("dato", "valor");
        for (var i = 0; i < (formulario.find('input[type=file]').length); i++) { 
           // formData.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')')[0]).files[0]));
            id = (formulario.find('input[type="file"]:eq('+i+')').attr("name"));
            file = ((formulario.find('input[type="file"]:eq('+i+')')[0]).files[0]);
            formData.append('archivo',file);

        }

         if (file == null)
            {
                alert("Debe seleccionar un archivo");
            }
        else { 


            url=url+id;
            $.ajax({     
                  
               url: url,
               type: 'POST',          
               contentType: false, 
               data: formData,     
               processData:false,
               cache: false,
            beforeSend : function (){
              
              //$('#cargando').show(300); 

                 $(".fileinput-remove-button").click();
                 alert("Se ha subido el archivo");
                 $(".download-"+id).disabled=false;
                 location.href = '../Controlador_digitador/verBitacoras';


            
            },
            success: function(data){
              // href.location = "";
              /*$('#cargando').hide(300);
              
              $('#fotos').append(data);
              
              $('#subida')[0].reset();
              
              return false;*/
            }
            
          });
        }

  
        });
   });
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });
</script>







