<!-- CONSTRUCCION DEL FORMULARIO--> 
<br>
  <div class="container">
        <div class="row">
                <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Editar reserva de Vehículo</h3>
                </div>
 <!-- esta es la forma de pasar los datos mediante un formulario -->
  <form class="form-inline" role="form" action="../editarReserva" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
   <div class="container">
      <div class="row">

<?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                              
                   
          
  <br>
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" > ID agenda: </label>
    <input class="form-control" id="id_agenda" name ="id_agenda" type="text" value="<?=$row->id_agenda?>" readonly >
    </div>
  </div>

 <div class="col-md-4">
            <div class="form-group">
            <label for="focusedInput" >Nombre Conductor: </label>
                <select class="form-control" id="nombre_conductor" style=" border-width:2px;" name="nombre_conductor">
                <!--rellenado con jquery-->                   
            </select>
            </div>
        </div>
        
        <div class="col-md-4">
          <div class="form-group">
            <label for="focusedInput" >Rut Conductor: </label>
            <select class="form-control" id="rut_conductor" style=" border-width:2px;" name="rut_conductor">
                <!--rellenado con jquery-->               
            </select>    
           </div>
        </div>

        <br>
        <br>
        <br>

         <div class="col-md-4">
            <div class="form-group">
              <label for="focusedInput" >Vehículo: </label>
              <select class="form-control" id="patente_vehiculo" style=" border-width:2px;" name="patente_vehiculo">
              <!--rellenado con jquery-->                   
              </select>    
          </div>
        </div>



    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Fecha: </label>
    <input class="form-control" id="datepicker" name="fecha" type="text" value="<?=$row->fecha?>">
    </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Horario: </label>
        <select class="form-control" id="horario" name="horario" value="<?=$row->horario?>">
                    
                    <option value="Media Jornada - Mañana">Media Jornada - Mañana</option>
                    <option value="Media Jornada - Tarde">Media Jornada - Tarde</option>
                    <option value="Jornada Completa">Jornada Completa</option>
                    <option value="Màs de una Jornada">Más de una Jornada</option>
                    
         </select>
        </div>
    </div><!--3ero-->

    <br>
    <br>
    <br>


    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Destino: </label>
    <input class="form-control" id="destino" name="destino" type="text" value="<?=$row->destino?>">
    </div>
    </div>

   
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Estado: </label>
    <select class="form-control" id="estado" name="estado" disabled>
                
                <option value="1">activo</option>
                <option value="0">inactivo</option>
              
     </select>
    </div>
    </div>
    
    <div class="col-md-4">
    <div class="form-group">
    <label for="focusedInput" >Rut Digitador: </label>
    <input class="form-control" id="rutdigitador" name ="rutdigitador" type="text" value="<?=$row->rut_usuario?>" readonly>
    </div>
    </div>



    <br>
    <br>
    <br>

    <div class="col-xs-10">
    <div class="form-group ">

    <label for="focusedInput" >Observaciones: </label>
    
    <textarea  class="form-control"  id="observaciones" name="observaciones" cols="150" rows="5" placeholder="Las observaciones son obligatorias"><?=$row->observaciones?></textarea>
    </div>
    </div>

  <?php endforeach;?> 
   
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
               
       <div class="container">
        <div class="fluid-row">

      <div class="form-group">
      <div class="col-xs-offset-7 col-md-10">
           <button id="botonAgregar"type="submit"  style ="width:450px;" class="btn btn-success btn-lg">Guardar edición </button>
      </div>
      </div>
    

      <br><br>

     
     </form>



      </div>

       </div>




      </div>



</div>
</div>
                     

</div>
</div>
</div>
</div>

<script type="text/javascript">
   $(document).ready(function (){
       $("#rut_conductor").click(function(e){
        var option = $('option:selected', this).text();   
        $("#nombre_conductor").val(option).attr('selected',true);  
   
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#nombre_conductor").click(function(e){
        var option = $('option:selected', this).text();   
        $("#rut_conductor").val(option).attr('selected',true);
    
     });
 });
</script>



<script type="text/javascript">
       $(document).ready(function (){               
         var url= "<?php echo base_url().'index.php/Principal/getConductores'?>";
       
         $.ajax({                  
                url: url,
                type: "POST",
                dataType: "JSON",
 
                success: function(data)  {
                //Limpiamos el select
                $('#nombre_conductor').empty();
                //Recorremos los vectores, generamos y agregamos los option
                //con los datos de los profesores y empresas respectivamente
                //Agregamos opciones de los nombres de conductores que existen
                
                $('#nombre_conductor').append('<option value="Sin información">Sin información</option>');

                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {   
                        $('#nombre_conductor').append("<option value='"+data[i].Rut+"'>"+data[i].Nombre+"</option>");
                     }          
                }
                $('#rut_conductor').append('<option value="Sin información">Sin información</option>');
               //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {  
                        $('#rut_conductor').append("<option value='"+data[i].Nombre+"' >"+data[i].Rut+"</option>");
                     } 
                }
                 $('#patente_vehiculo').append('<option value="Sin información">Sin información</option>');
                //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="vehiculo") {  
                         $('#patente_vehiculo').append("<option value='"+data[i].Patente+"'>"+data[i].Patente+"</option>");
                     } 
                }
 
                },
                error: function(result) {
                console.log("Error: " + result.statusText);
                }

          });
    });       
</script>


    
<script type="text/javascript">
        $(document).ready(function() {
           $("#datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
});
</script>


<!--CAMBIO DE ESTILOS EN CSS PARA INPUTS-->
<style type="text/css">
div {
   margin: .4em 0,0,0;
}
div label {
  width: 50%;
  float: left;
}
</style>
