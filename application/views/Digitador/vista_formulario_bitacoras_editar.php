<!-- CONSTRUCCION DEL FORMULARIO--> 
<br>
  <div class="container">
        <div class="row">
                <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Editar Bitácora</h3>
                </div>
 <!-- esta es la forma de pasar los datos mediante un formulario -->
<form class="form-inline" role="form" action="../editarBitacora" method="post"> <!-- acá ingreso el controlador para que pasen los datos al modelo -->
   
   <div class="container">
      <div class="row">
    
    <br>

    <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                              
                   
                                      
        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" > N° bitácora: </label>
        <input class="form-control" id="n_bitacora" name ="n_bitacora" value="<?=$row->n_bitacora?>" type="text" style=" border-width:2px;" readonly>
        </div>
      </div>

       <div class="col-md-4">
            <div class="form-group">
            <label for="focusedInput" >Nombre Conductor: </label>
                <select class="form-control" id="nombre_conductor" style=" border-width:2px;" name="nombre_conductor" value="">
            <!--rellenado con jquery-->                   
            </select>
            </div>
        </div>
        
        <div class="col-md-4">
          <div class="form-group">
            <label for="focusedInput" >Rut Conductor: </label>
            <select class="form-control" id="rut_conductor" style=" border-width:2px;" name="rut_conductor">
                <!--rellenado con jquery-->               
            </select>    
           </div>
        </div>

        <br>
        <br>
        <br>

         <div class="col-md-4">
            <div class="form-group">
              <label for="focusedInput" >Vehículo: </label>
              <select class="form-control" id="patente_vehiculo" style=" border-width:2px;" name="patente_vehiculo">
              <!--rellenado con jquery-->                   
              </select>    
          </div>
        </div>


        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Fecha: </label>
        <input class="form-control" id="datepicker" name="fecha" type="text" style=" border-width:2px;" value ="<?= $row->fecha?>" required>
        </div>
        </div>

        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Destino</label>
        <input class="form-control" id="destino" name="destino" type="text" style=" border-width:2px;" 
        value="<?= $row->destino?>" required>
        </div>
        </div>

       

        <br>
        <br>
        <br>
        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >KM salida: </label>
        <input class="form-control" id="kmsalida" name="kmsalida" type="text" style=" border-width:2px;" value="<?= $row->km_salida?>" required>
        </div>
        </div>


        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >KM llegada: </label>
        <input class="form-control" id="kmllegada" name="kmllegada" type="text" style=" border-width:2px;" value="<?= $row->km_llegada?>"required>
        </div>
        </div>


        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >KM recorridos: </label>
        <input class="form-control" id="kmrecorridos" name="kmrecorridos" type="text" style=" border-width:2px;" value="<?= $row->km_Recorridos?>" required>
        </div>
        </div>


         <br>
        <br>
        <br>


        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Carga de combustible: </label>
        <input class="form-control" id="combustible"  name="combustible" type="text"  placeholder="Si no carga se ingresa 0" style=" border-width:2px;" value="<?= $row->carga_Combustible?>" required>
        </div>
        </div>





        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Estado: </label>
        <select class="form-control" id="estado" style=" border-width:2px;" name="estado">
                    
                    <option value="0">activo</option>
                    <option value="1">inactivo</option>
                  
         </select>
        </div>
        </div>

        <div class="col-md-4">
        <div class="form-group">
        <label for="focusedInput" >Rut Digitador: </label>
        <input class="form-control" id="rutdigitador" name ="rutdigitador" style=" border-width:2px;" type="text" value="<?=$row->rut_usuario?>" readonly>
        </div>
        </div>


        <br>
        <br>
        <br>

        <div class="col-xs-10">
        <div class="form-group ">

        <label for="focusedInput" >Observaciones: </label>
        
        <textarea  class="form-control"  id="observaciones" name="observaciones" cols="150" rows="5"
         ><?=$row->observaciones?></textarea>
        </div>
        </div>
     <?php endforeach;?> 

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
     <br>
    
   
             <div class="container">
              <div class="fluid-row">

            <div align="central">
            <div class="col-xs-offset-3 col-md-10">
                 <button id="botonAgregar"type="submit"  style ="width:450px;" class="btn btn-success btn-lg">Guardar edición </button>
            </div>
            </div>
          



           
           
             </div>
            </div>


<br>
<br>

  


</div>
</div>
  </form>

                       

</div>
</div>
</div>
</div>

                       <!-- <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-10">
                        <a id= "btnCancelar" href="../Controlador_digitador/verBitacoras">
                        <button id="botonCanvelar"type="submit" class="btn btn-danger">Cancelar </button>
                        </a>
                        </div>

                        </div>-->







<!--CARGA PLUGINS DATEPICKER PARA DESPLEGAR CALENDARIO PARA FECHAS-->
<script>
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
$("#datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
});
</script>

    <!--CAMBIO DE ESTILOS EN CSS PARA INPUTS-->
    <style type="text/css">
    div {
       margin: .4em 0,0,0;
    }
    div label {
      width: 50%;
      float: left;

    }


    </style>

  <script type="text/javascript">
   $(document).ready(function (){
       $("#rut_conductor").click(function(e){
        var option = $('option:selected', this).text();   
        $("#nombre_conductor").val(option).attr('selected',true);  
   
     });
 });
</script>

<script type="text/javascript">
   $(document).ready(function (){
       $("#nombre_conductor").click(function(e){
        var option = $('option:selected', this).text();   
        $("#rut_conductor").val(option).attr('selected',true);
    
     });
 });
</script>

<script type="text/javascript">
       $(document).ready(function (){               
         var url= "<?php echo base_url().'index.php/Principal/getConductores'?>";
       
         $.ajax({                  
                url: url,
                type: "POST",
                dataType: "JSON",
 
                success: function(data)  {
                //Limpiamos el select
                $('#nombre_conductor').empty();
                //Recorremos los vectores, generamos y agregamos los option
                //con los datos de los profesores y empresas respectivamente
                //Agregamos opciones de los nombres de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {   
                        $('#nombre_conductor').append("<option value='"+data[i].Rut+"'>"+data[i].Nombre+"</option>");
                     }          
                }
               //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="conductor") {  
                        $('#rut_conductor').append("<option value='"+data[i].Nombre+"' >"+data[i].Rut+"</option>");
                     } 
                }
                //Agregamos opciones de los rut de conductores que existen
                for (i = 0; i < data.length; i++) { 
                     if (data[i].Categoria=="vehiculo") {  
                        $('#patente_vehiculo').append("<option value='"+data[i].Patente+"'>"+data[i].Patente+"</option>");
                     } 
                }
 
                },
                error: function(result) {
                console.log("Error: " + result.statusText);
                }

          });
    });       
</script>


