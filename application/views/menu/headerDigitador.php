<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>imagen/icono.ico">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
    <link href="<?=base_url()?>css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?=base_url()?>css/fileinput.css"  rel="stylesheet" type="text/css" media="all" />
    <link href="<?=base_url()?>css/uploadfile.css" rel="stylesheet">  
    <link href="<?=base_url()?>css/jquery-ui.css" rel="stylesheet">    

    <!-- Llamada a componentes de Javascript -->
    <script type="text/javascript" src="<?=base_url()?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>js/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery.dataTables.js"></script>      <!--PLUGIN DATATABLE-->
    <script type="text/javascript" src="<?=base_url()?>js/fileinput.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url()?>js/uploadfile.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery-ui.js"></script>

   
     <!--Fin de llamadas-->
    <title>Inicio Digitador</title>

</head>
<img src="<?=base_url()?>imagen/logo1.jpg" width='1366' height='110'>  
<nav role="navigation" class="navbar navbar-default">
      <div class="navbar-header">
        <a href="<?php echo base_url();?>index.php/digitador/Controlador_digitador" class="navbar-brand" ><i class="glyphicon glyphicon-user"></i> Digitador </a> <!-- aquí va en href el controlador que direcciona a la vista-->
      </div> 
       <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
            
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href=""> Bitácoras <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a><?php echo anchor("Digitador/Controlador_digitador/verBitacoras","Digitar Bitácoras")?></a></li>
                        <li><a><?php echo anchor("Digitador/Controlador_digitador/verBitacorasDeshabilitadas","Habilitar Bitácoras")?></a></li>
                    </ul>
                </li>
                    <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href=""> Reservas <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a><?php echo anchor("Digitador/Controlador_digitador/verReservas","Digitar Reservas")?></a></li>
                        <li><a><?php echo anchor("Digitador/Controlador_digitador/verReservasDeshabilitadas","Habilitar Reservas")?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li  class="pull-right" >
                      <a href="<?php echo base_url();?>index.php/Principal/cerrarSesion"><i class="glyphicon glyphicon-off"></i> Cerrar sesión</a>
                 </li> 
           </ul>
      </div>    

  </nav>

<body>

 <div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">© 2016 - Ilustre Municipalidad De Cauquenes -DIDECO.
          
     </p>
      
      
    </div>
    </div>

</html>