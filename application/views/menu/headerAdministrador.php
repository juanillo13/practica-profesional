<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>imagen/icono.ico">
    
     <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
    <link href="<?=base_url()?>css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all"/>
    
    <!-- Para que se despliegue el menu -->
    <script type="text/javascript" src="<?=base_url()?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>js/jquery.dataTables.js"></script>      <!--PLUGIN DATATABLE-->
    <script type="text/javascript" src="<?=base_url()?>js/jquery-confirm.min.js"></script>


    <title>Inicio Administrador</title>

 


  </head>
  
<img src="<?=base_url()?>imagen/logo1.jpg" width='1366' height='110'>
  
    <nav role="navigation" class="navbar navbar-default" >
        <div class="navbar-header">            
            <a href="<?php echo base_url();?>index.php/administrador/Controlador_administrador" class="navbar-brand" ><i class="glyphicon glyphicon-user"></i> Administrador</a> <!-- aquí va en href el controlador que direcciona           a la misma vista-->
        </div>
 
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
     
            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Vehículos<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                
                    <li><a><?php echo anchor("administrador/Controlador_administrador/MantenedorVehiculos","Mantenedor de Vehículos")?></a></li>
                    <li><a><?php echo anchor("administrador/Controlador_Vehiculos/verVehiculosDeshabilitados","Habilitar Vehículos")?></a></li>
                    <li><a><?php echo anchor("administrador/Controlador_administrador/cargarBuscarVehiculo","Hoja de Trabajo")?></a></li>
                </ul>
            </li>
                
            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"> Conductores <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a> <?php echo anchor("administrador/Controlador_administrador/MantenedorConductores","Mantenedor de conductores")?></a></li>
                    <li><a> <?php echo anchor("administrador/Controlador_Conductor/verConductoresDeshabilitados","Habilitar Conductor")?></a></li>
                 </ul>
            </li>

            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"> Bitácoras <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a> <?php echo anchor("administrador/Controlador_administrador/MantenedorBitacoras","Mantenedor de bitácoras")?></a></li>
                     <li><a> <?php echo anchor("administrador/Controlador_Bitacoras/verBitacorasDeshabilitadas","Habilitar Bitácoras")?></a></li>
                </ul>
            </li>

            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href=""> Reservas <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a> <?php echo anchor("administrador/Controlador_administrador/MantenedorReservas","Mantenedor de Reservas ")?> </a></li>
                    <li><a><?php echo anchor("administrador/Controlador_Reservas/verReservasDeshabilitadas","Habilitar Reservas")?></a></li>
                </ul>
            </li>

            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="">Reportes <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                     <li><a> <?php echo anchor("administrador/Controlador_administrador/Reportes","Gestión de Reportes")?></a></li>
                </ul>
            </li>
            
            <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="">Usuarios <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a> <?php echo anchor("administrador/Controlador_administrador/MantenedorUsuarios","Mantenedor de Usuarios")?></a></li>
                    <li><a><?php echo anchor("administrador/Controlador_Usuario/verUsuariosDeshabilitados","Habilitar Usuarios")?></a></li>
                </ul>
            </li>

           <!-- <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="">Cerrar sesión <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a> <?php echo anchor("Principal/cerrarSesion","Cerrar Sesión")?></a></li>
                        <li><a> href=""></a></li>
                    </ul>
            </li>-->
        </ul>
            <ul class="nav navbar-nav navbar-right">
                <li  class="pull-right" >
                    <a href="<?php echo base_url();?>index.php/Principal/cerrarSesion"><i class="glyphicon glyphicon-off"></i> Cerrar sesion</a>                    </a> 
                </li> 
           </ul>
 

    </nav>

  <body>

    <div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">© 2016 - Ilustre Municipalidad De Cauquenes -DIDECO.
          
     </p>
      
      <!--<a href="http://youtu.be/zJahlKPCL9g" class="navbar-btn btn-danger btn pull-right">
      <span class="glyphicon glyphicon-star"></span>  Subscribe on YouTube</a>-->
    </div>
    
    
  </div>

</html>


