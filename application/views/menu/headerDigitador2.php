<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>imagen/icono.ico">
    
     <!-- Llamadas css -->
    <link href="<?=base_url()?>css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
    <link href="<?=base_url()?>css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all"/>


    <!-- Llamada js-->
    <script type="text/javascript" src="<?=base_url()?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>js/jquery-confirm.min.js"></script>

   
    <title>Inicio Administrador</title>


  </head>
  <img src="<?=base_url()?>imagen/logo1.jpg" width='1366' height='110'>

  <body>
    <nav role="navigation" class="navbar navbar-default">
       <ul class="nav navbar-nav">
        <div class="navbar-header">            
            <a href="<?php echo base_url();?>index.php/digitador/Controlador_digitador2" class="navbar-brand" ><i class="glyphicon glyphicon-user"></i> Digitador </a> <!-- aquí va en href el controlador que direcciona  a la misma vista-->
        </div>
        <!--<ul role="menu" class="nav navbar-nav navbar-right">
           <li><a href="../Principal/cerrarSesion"><i class="glyphicon glyphicon-off "></i>Cerrar Sesión </a> </li>
        </ul>-->
      </ul>
    <ul class="nav navbar-nav navbar-right">
            <li  class="pull-right" >
               <a href="<?php echo base_url();?>index.php/Principal/cerrarSesion"><i class="glyphicon glyphicon-off"></i> Cerrar sesión</a>
         
            </li> 
    </ul>        
 
  </nav>  

    <div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">© 2016 - Ilustre Municipalidad De Cauquenes -DIDECO.
          
     </p>
      
    
    </div>
    
    
  </div>
