
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>imagen/icono.ico">
    <title>Login</title>

    <!-- CARGA CSS -->
    <link href="<?=base_url()?>css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
    <link href="<?=base_url()?>css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all"/>
   
    <!-- CARGA JS-->
    <script type="text/javascript" src="<?=base_url()?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>js/jquery-confirm.min.js"></script>
    
  </head>

  <body>

    
<img src="<?=base_url()?>imagen/logo1.jpg" width='1366' height='110'>


  <div class="container">
    <div class="container" role="form">
          <h2 class="form-signin-heading text-center">Sistema de Gestión de Vehículos Municipales</h2> 
    </div>
        
<br></br>
<br></br>
    <?php echo form_open('Principal/verificar');?>
      <form class="form-horizontal" >
        <div class="row">
         <div class="col-md-4"></div>
         <div class="col-md-4">
          <div class="panel panel-primary">
          <div class="panel-heading">
         <h3 class="panel-title">Ingreso a sistema</h3></div>
          <div class="panel-body">
              <br></br>
              
              <label for="Username"  class="sr-only">Rut</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-user"></span>
                    </span>
                    <input type="text" id="Username" name="Username" class="form-control" placeholder="Ingrese Rut sin guión y sin puntos  " required autofocus>
                 </div>
              <br>
              <label for="Password" class="sr-only">Password</label>
                   
                  <div class="input-group">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-lock"></span>
                    </span>
                        <input type="password" name="Password" id="Password" class="form-control" placeholder="Ingrese Contraseña" >
                 </div>
       
              <br>
             <button id="ingresar" class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
          </div>

           <div class="col-md-4"></div>
          </div>
      </form>
  </div>
</div>
              
      </div>
      
 <?php echo form_close();?>

    </div> <!-- /container -->

 
<style type="text/css">
  body{

  background: #F7F7F7;

  }


</style>


 <script type="text/javascript">
 $(document).ready(function(){
      $("#Username").keypress(function(tecla) {
      
         if ($(this).val().length<9 && ((tecla.charCode > 47 && tecla.charCode <= 57) || (tecla.charCode ==107 || tecla.charCode==75)) ) {                   // Más del límite de caracteres?
    
         }
         else{
          tecla.preventDefault();
         }
    });
});
</script>
  
   
  </body>
</html>