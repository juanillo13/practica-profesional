<br>
<body>
  
      <div class="container">
      
                <div class="panel panel-primary">
                  <div class="panel-heading">
                  <h3 class="panel-title">Actividades Mensuales</h3>
                 </div>
     
      <table class="table table-hover" >
        <thead>
           
        </thead>
        <tbody>
          <!--ultimo reporte-->
        <td> 
            <form class="form-horizontal" action="" method="post">
            <div class="form-group">
             <label class="control-label col-xs-2">Actividades Mensuales</label>
        
            <div class="control-label col-xs-2">

                <select class="form-control" id = "opcionPatente"name ="opcionPatente">
                  <?php foreach($resultado as $row):?>

                    <option  value="<?= $row->patente?>"> <?= $row->patente?> </option>
                          <?php endforeach;?>
                </select>
            </div>
                                
                          
        
                     <div class="control-label col-xs-2">
                        <select class="form-control" id="opcionMes" name="opcionMes">
                            <option value="1">Enero </option>
                            <option value="2">Febrero </option>
                            <option value="3">marzo </option>
                            <option value="4">abril </option>
                            <option value="5">mayo </option>
                            <option value="6">junio </option>
                            <option value="7">julio </option>
                            <option value="8">agosto </option>
                            <option value="9">septiembre </option>
                            <option value="10">octubre </option>
                            <option value="11">noviembre </option>
                            <option value="12">diciembre</option>
                        </select>
            </form> 
            </td>

            <td>           
             <button id="buscar" type="submit" class="btn-md btn btn-success">Buscar </button>
            </td>                    
       </tbody>   
    </table> 
    </div>  
  </div>            
    <br>
    <br>
  
    <div id ="mensuales" class="container" >
     <div class="row">
           <div class="panel panel-primary">
                  <div  class="panel-heading">
                     <h3 class="panel-title">Datos mensuales</h3>
                      <div id="divPatente">
                          <!--Se ingresará la patente del vehiculo-->
                     </div>  
                   </div>       
          <div class="panel-body">
           <table id = "tablaMensuales" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" >
               <form action="achurarMensuales" method="POST">
                    <thead>  
                           <tr>
                                  <th colspan="1">Nº Bitacora</th>
                                  <th colspan="1">Patente</th>
                                  <th colspan="1">Rut conductor</th>
                                  <th colspan="1">Nombre conductor</th>
                                  <th colspan="1">Fecha</th>
                                  <th colspan="1">Destino</th>
                                 
                              </tr>
                      </thead> 
                      
                      <tbody > 
 
                      <!--Dinamicamente se le agrega  información-->
                                   
                    </tbody>
                    </form> 
               </table>
                  
             </div>
          </div>
      </div>
  </div>

                     
        

           

  
        </body>
</html>


<script type="text/javascript">
       $(document).ready(function (){
          
           $("#buscar").click(function(e){
                  var url = "<?php echo base_url().'index.php/administrador/Controlador_Bitacoras/bitacorasMensuales'?>";
                  $("#tablaMensuales").find("tbody").empty();  
                  var patente = $("#opcionPatente").val();
                  var mes = $("#opcionMes").val();
                  var anio = "2016";
                  var data = $(this).serializeArray();
                  data.push({name:'patente',value:patente});
                  data.push({name:'mes',value:mes});
                  data.push({name:'anio',value:anio});
 
                  $("#tablaMensuales").hide("fast");
                  $("#tablaMensuales").show("slow");
                  $("#divPatente").empty();
                  $("#divPatente").append("<h5><label >Patente: "+patente+"</label></h5>")
                
               
                  $.ajax({
              
                        url: url,
                        data : data,
                        type: "POST",
                        dataType : 'JSON',


                       //Obtenemos los valores de los ramos del alumno, si esta aprobado o no.
                        success: function(data)  {
                           
                        if (data.length>0) { 
                          for (var i = 0 ; i<= data.length; i++)  {

                                $("#tablaMensuales").append("<tr><td>"+data[i].n_bitacora+"</td><td>"+data[i].patente+"</td><td>"+data[i].rut_conductor+"</td><td>"+data[i].nombre_conductor+"</td><td>"+data[i].fecha+"</td><td>"+data[i].destino+"</td></tr>");
                            };
                         }
                         else {
                          alert("No existen registros para estos datos");
                         }
                        },

                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
   
          });

       });
</script>
