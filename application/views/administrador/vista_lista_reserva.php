<br>
<br>
<div class="container" style="">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Lista de Reservas</h3> 
                   </div>
                      


          
          <!-- tabla que muestra los datos del vehiculo-->
              <table id="tabla_dinamica" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover"   >
                  <thead> 
                    <tr>
                              <th>ID</th>
                              <th>Conductor</th>
                              <th>Rut Conductor</th>
                              <!--<th>Rut Digitador</th>-->
                              <th>Patente Vehiculo</th>
                              <!--<th>Km salida</th>--> <!-- nota: No es necesario mostrar el estado en la tabla-->
                             <!-- <th>Km LLegada</th>
                              <th>Km Recorridos</th>-->
                              <th>Fecha</th>
                              <th>Destino</th>
                              <th>Horario</th>
                              <!--<th>Carga de Combustible</th>
                              <th>Observaciones</th>-->
                              <th>Opciones</th>

                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                                                
                                 <td> <?=$row->id_agenda?></td> 
                                 <td> <?=$row->nombre_conductor?> </td>
                                 <td> <?=$row->rut_conductor?> </td>
                                 <!--<td> <?=$row->rut_usuario?> </td>-->
                                 <td> <?= $row->vehiculo_patente?> </td>
                                 <!--<td> <?= $row->km_salida?> </td>
                                 <td> <?= $row->km_llegada?> </td>
                                 <td> <?= $row->km_Recorridos?> </td>-->
                                 <td> <?= $row->fecha?> </td>
                                 <td> <?= $row->destino?> </td>
                                 <td> <?= $row->horario?> </td>
                                 <!--<td> <?= $row->carga_Combustible?> </td>
                                 <td> <?= $row->observaciones?> </td>-->

                              
                                  <td class="col-lg-4"> 

                                 
                                   
                                    <a href="<?=$row->id_agenda?>" class="verReserva">
                                      <button  type="button" class="btn btn-primary " data-toggle="modal" data-target="#ModalEditar">
                                              <i class="glyphicon glyphicon-search"></i> 
                                        Detalle
                                      </button>
                                    </a>



                                    <a href="<?php echo base_url().'index.php/administrador/Controlador_Reservas'?>/eliminarReserva/<?=$row->id_agenda?>" class="eliminarReserva">
                                      <button  type="button" class="btn btn-warning " data-toggle="modal">
                                              <i class="glyphicon glyphicon-trash"></i> 
                                        Eliminar
                                      </button>
                                    </a>

                                    <a href="<?php echo base_url().'index.php/administrador/Controlador_Reservas'?>/eliminarReservasfisica/<?=$row->id_agenda?>" class="eliminarReservaFisica"> <!--tengo que crear la funcion eliminar-->
                                    <button   type="button" class="btn btn-danger " data-toggle="modal"><i class="glyphicon glyphicon-trash"></i> 
                                     Eliminar 
                                    </button>
                                    </a>
                                   
                                   
                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
      
      </div>
   </div>
</div>
</div>



<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Detalle</h4>
          </div>
          <div class="modal-body">

               <form  id="formEditar" action ="" method="POST"> <!---->
                      <div>
                           <div align="center" > 
                            id:
                            <input id="Aid_edit" type="text"  name="bitacora_" class="form-control" disabled />                                                            
                            Nombre del Conductor:
                            <input id="Anombre_edit" type="Text" name="nombre_" class="form-control" disabled />
                            Rut del Conductor:
                            <input id="ArutC_edit" type="Text" name="rutC_" class="form-control" disabled />
                           
                            Patente del Vehículo:
                            <input id="Apatente_edit" type="Text" name="patente_" class="form-control" disabled />
          
                             Fecha:
                            <input id="Afecha_edit" type="Text" name="fecha_" class="form-control" disabled />
                              
                             Horario :
                            <input id="Ahorario_edit" type="Text" name="horario_" class="form-control" disabled />
                             
                             Destino:
                            <input id="Adestino_edit" type="Text" name="destino_" class="form-control" disabled />
                             
                             Rut del Digitador:
                            <input id="ArutD_edit" type="Text" name="rutD_" class="form-control" disabled />
                             
                             Observaciones:
                          <!--<input id="Aobs_edit" type="Text" name="obs_" class="form-control" disabled />-->
                            <textarea  id="Aobs_edit" name="obs_"   class="form-control"  cols="110" rows="5" disabled></textarea>
                            <!--Estado:
                            <select id= "Aestado_edit"name="Aestado_edit" class="form-control"> 
                                <option value="0">Habilitado</option> 
                                <option value="1">Deshabilitado</option>  
                            </select> -->

                            <br />
                            <br />
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        
                        <!--<button type="submit" class="btn btn-primary">Guardar cambios</button>-->
                      </div>
                        </div>
                  </div>
              </form>
          </div>
      </div>
    </div>
</div>





<script type="text/javascript">
       $(document).ready(function (){
           $("a").click(function(e){
             //  e.preventDefault();   
              var clase= $(this).attr("class");

             if (clase =="verReserva") {
                 e.preventDefault();                         //Evita que el evento se genere (redireccionar)
                 var url = "<?php echo base_url().'index.php/administrador/Controlador_Reservas'?>"+"/detalleReserva/"; 
                 var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
             
                $.ajax({
                        url: url+link,
                        type: "POST",
                        //data : data,
                        dataType : 'JSON',                    //Se define del tipo JSON ya que recibiremos datos
                                                    //Solo se envían datos, por eso no usamos el parámetro "Data:"
                        success: function(data)  {
                               $('#Aid_edit').val(data.id_agenda);
                               $('#Anombre_edit').val(data.nombre_conductor);
                               $('#ArutC_edit').val(data.rut_conductor);
                               
                               $('#Apatente_edit').val(data.vehiculo_patente);
                               
                               $('#Afecha_edit').val(data.fecha);
                                $('#Ahorario_edit').val(data.horario);
                               $('#Adestino_edit').val(data.destino);
                               $('#ArutD_edit').val(data.rut_usuario);
                               
                               $('#Aobs_edit').val(data.observaciones);
  
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
              }
   
              if (clase=="eliminarReserva"){

                  var answer = confirm("¿Seguro que deseas deshabilitar?","si");



                 
                if(answer)  //cuando es verdadera, o sea, que desea eliminar 
                  {
  
                   }
                   else {
                      e.preventDefault();                         //Evita que el evento se genere (redireccionar)

                   }
                }// fin if
                if (clase=="eliminarReservaFisica"){

                  var answer = confirm("¿Seguro que deseas Eliminar?","si");



                 
                if(answer)  //cuando es verdadera, o sea, que desea eliminar 
                  {
   
                   }
                  else {
                    e.preventDefault();                         //Evita que el evento se genere (redireccionar)

                  }
                }// fin if



            });
       });
</script>





<script type="text/javascript" charset="utf-8"> 
 $(document).ready(function() {
            $('#tabla_dinamica').DataTable({
                  "language": {
                      "lengthMenu": "Display _MENU_ records per page",
                      "zeroRecords": "No se encontró ninguna entrada",
                      "info": "Mostrando _PAGE_ de _PAGES_",
                      "infoEmpty": "No hay registros disponibles",
                      "infoFiltered": "(Filtrado de los registros totales _MAX_ )"
                   }
              });
              $("div#tabla_dinamica_length").empty();
              $("div#tabla_dinamica_paginate").empty();
              $("div#tabla_dinamica_info").empty();
              $("div#tabla_dinamica_wrapper").removeClass();
              $("div#tabla_dinamica_wrapper").addClass("form-group");
              $("div#tabla_dinamica_filter").find("input").addClass("form-control");
              $("div#tabla_dinamica_filter").removeClass();
              $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
              //$("div#tabla_dinamica_filter").find("label").remove();
             // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
              $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
              $(".dataTables_paginate").empty();
              $(".paging_simple_numbers").empty();
             
          });
</script> 
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>