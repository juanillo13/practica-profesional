 <body>
  
 <br>
 <br>
 <br>
 <h3 align="center">Actividades diarias de vehículos Municipales</h3>
   <div class="container"  style="">
     <div class="row" >
           <div class="panel panel-primary" >
                  <div class="panel-heading">
                   <h3 class="panel-title">Tabla de Vehículos</h3> 
                 </div>        
            </div>

          
         	<div class="table-responsive" style="">
              <table class="table table-bordered table-hover" id="tabladinamica"  >
                  <thead> 
                    <tr>
                              <th>Patente Vehiculo</th>
                              <th>Conductor</th>
                              <th>Rut Conductor</th>
 	                            <th>Fecha</th>
                              <th>Destino</th>
                              <!--<th>Estado</th>-->

                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                <td> <?= $row->vehiculo_patente?> </td>                            
                                <td> <?=$row->nombre_conductor?> </td>
                                <td> <?=$row->rut_conductor?> </td>
                                <td> <?= $row->fecha?> </td>
                                <td> <?= $row->destino?> </td>
                                              
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
       </div>
      </div>
   </div>
  </div>
  </body>
</html>