<div class="container" style="padding-top: 80px;">
 <div class="row">
   <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Vehículos deshabilitados</h3> 
    </div>
    <!-- tabla que muestra los datos del vehiculo-->
    <table id="tabla_dinamica" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-responsive"   >
      <thead> 
        <tr>
          <th>Patente</th>
          <th>Descripción</th>
          <th>Tipo</th>
          <th>Año</th>
          <th>Color</th>
          <th>Responsable</th> <!-- nota: No es necesario mostrar el estado en la tabla-->
          <th>Opciones</th>
              </tr>
      </thead> 
        <tbody> 
       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
       <tr>
             <td> <?=$row->patente?> </td> 
         <td> <?=$row->descripcion?> </td>
         <td> <?=$row->tipo?> </td>
         <td> <?=$row->año?> </td>
         <td> <?=$row->color?> </td>
         <td> <?=$row->responsable?> </td>
              <td class="col-lg-4"> <!--Botones para acciones de editar y eliminar, van dentro de la tabla-->

                       <a href="habilitarVehiculo/<?=$row->patente?>" class=""> 
            <button   type="button" class="btn btn-danger"><i class="glyphicon glyphicon-check"></i> 
             Habilitar
           </button>
         </a>
      </td>
     </tr> 
   <?php endforeach;?>                 
 </tbody>
 </table>
</div>
</div>
</div>






<script type="text/javascript" charset="utf-8"> 
$(document).ready(function() {
  $('#tabla_dinamica').DataTable({
    "language": {
      "lengthMenu": "Display _MENU_ records per page",
      "zeroRecords": "No se encontró ninguna entrada",
      "info": "Mostrando _PAGE_ de _PAGES_",
      "infoEmpty": "No hay registros disponibles",
      "infoFiltered": "(Filtrado de los registros totales _MAX_ )"
    }
  });
  $("div#tabla_dinamica_length").empty();
  $("div#tabla_dinamica_paginate").empty();
  $("div#tabla_dinamica_info").empty();
  $("div#tabla_dinamica_wrapper").removeClass();
  $("div#tabla_dinamica_wrapper").addClass("form-group");
  $("div#tabla_dinamica_filter").find("input").addClass("form-control");
  $("div#tabla_dinamica_filter").removeClass();
  $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
              //$("div#tabla_dinamica_filter").find("label").remove();
             // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
             $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
             $(".dataTables_paginate").empty();
             $(".paging_simple_numbers").empty();
                    });
</script> 
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
  margin: 0;
  padding: 0;
  border: none;
  box-shadow: none;
  text-align: center;
}
.kv-avatar .file-input {
  display: table-cell;
  max-width: 220px;
}
</style>