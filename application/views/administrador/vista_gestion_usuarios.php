<div class="container">
<div class="row ">
    <div class="col-md-3">    
       <div class="row">
                        
                      <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAgregarUsuario">
                                  <i class="glyphicon glyphicon-plus"></i> 
                                    Usuario
                          </button> 
 </div>
  </div>
 <div>


<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Usuarios</h3> 
                   </div>
                   
           

          <!-- tabla que muestra los datos del Conductor-->
              <table id="tabla_dinamica"  cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover"  >
                  <thead> 
                    <tr>
                              <th>Rut</th>
                              <th>Nombre</th>
                              <th>Email</th>
                              <th><a href="#" title="Tipos de Privilegios" data-toggle="popover" data-trigger="hover" data-content="
                              0 = Administrador
1 = Digitador Principal
2 = Digitador Secundario">Privilegio</a></th>
                              <th>Contraseña</th> <!-- nota: No es necesario mostrar el estado en la tabla-->
                              <th>Opciones</th>
                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                                                
                                 <td> <?=$row->rut?> </td> 
                                 <td> <?=$row->nombre?> </td>
                                 <td> <?=$row->email?> </td>
                                 <td> <?= $row->privilegio?> </td> 
                                 <td> <?= $row->contraseña?> </td>

                                 <!--<td> <?= $row->estado?> </td>-->
                              
                                  <td class="col-lg-4"> <!--Botones para acciones de editar y eliminar, van dentro de la tabla-->

                                 
                                    <a href="<?=$row->rut?>"class="editarUsuario">
                                      <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalEditar">
                                              <i class="glyphicon glyphicon-edit"></i> 
                                        Editar
                                      </button>
                                    </a>

                                    <a href="<?php echo base_url().'index.php/administrador/Controlador_Usuario'?>/eliminarUsuario/<?=$row->rut?>" class="eliminarUsuario"> <!--tengo que crear la funcion eliminar-->
                                    <button   type="button" class="btn btn-danger" data-toggle="modal"><i class="glyphicon glyphicon-trash"></i> 
                                     Eliminar
                                    </button>
                                    </a>
                                   
                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
      
      </div>
   </div>
</div>
</div>

 <!--********************************* MODAL EDITAR CONDUCTOR ***************************************-->

<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
          </div>
          <div class="modal-body">

               <form  id="formEditar" action ="../Controlador_Usuario/editarUsuario" method="POST"> <!-- en el acction iba EditarAlumno en el formualario del jota-->
                      <div>
                           <div align="center" > 
                            Rut:
                            <input id="Arut_edit" type="text" name="rut_" class="form-control" readonly />                                                            
                            Nombre:
                            <input id="Anombre_edit" type="Text" name="nombre_" class="form-control" required />
                            Email:
                            <input id="Aemail_edit" type="email" name="email_" class="form-control" required />
                            Privilegio:
                            <select id ="Aprivilegio_edit" name="privilegio_" class="form-control"> 
                            <option value="0">Administrador</option> 
                            <option value="1">Digitador</option>  
                            <option value="2">Digitador 2</option>
                                    
                            </select> 
                            Contraseña:
                            <input id="Acontraseña_edit" type="Text" name="contraseña_" class="form-control" required />
          
                            Estado:
                            <select id= "Aestado_edit"name="Aestado_edit" class="form-control"> 
                                <option value="0">Habilitado</option> 
                                <option value="1">Deshabilitado</option>  
                            </select> 

                            <br />
                            <br />
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Guardar cambios</button>
                      </div>
                        </div>
                  </div>
              </form>
          </div>
      </div>
    </div>
</div>






 <!--********************************* MODAL AGREGAR USUARIO***************************************-->
<div class="modal fade" id="ModalAgregarUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4  align="center" class="modal-title" id="myModalLabel">Ingreso Nuevo Usuario</h4>
          </div>
          <div class="modal-body">

               <form id="FormIngresarUsuario" action ="../Controlador_Usuario/agregarUsuario" method="POST">  <!-- le agrege el action al formulario-->

                  <div align="center" > 
                
                      Rut: 
                      <input id ="rut_" name="rut_" type="number"  class="form-control" required />                                                            
                      Nombre:
                      <input id ="nombre_" name="nombre_" type="text"  class="form-control" required  /> 
                      Email:
                      <input id="email_" type="email" name="email_" class="form-control" required />
                      Privilegio:
                      <select id ="privilegio_" name="privilegio_" class="form-control"> 
                            <option value="0">Administrador</option> 
                            <option value="1">Digitador</option>  
                            <option value="2">Digitador 2</option>
                                     
                            </select> 
                      Contraseña:
                      <input id="contraseña_" type="Text" name="contraseña_" class="form-control" required />
                      Estado:
                      <select id ="estado_" name="estado_" class="form-control"> 
                      <option value="0">Habilitado</option> 
                      <option value="1">Deshabilitado</option>  
                      </select> 
                      <br />
                      <br />
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                  <button  type="submit" class="btn btn-success">Agregar Usuario</button>            <!--Se elimina el valor SUBMIT ya que Cumpliría el aCtion-->
                </div>
                 
              </form>
          </div>
      </div>
    </div>
</div>


<script type="text/javascript">
       $(document).ready(function (){
           $("a").click(function(e){
                
              var clase= $(this).attr("class");

             if (clase =="editarUsuario") {
                 e.preventDefault();                         //Evita que el evento se genere (redireccionar)
                 var url = "<?php echo base_url().'index.php/administrador/Controlador_Usuario'?>"+"/buscarUsuario/"; 
                 var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
             
                $.ajax({
                        url: url+link,
                        type: "POST",
                        //data : data,
                        dataType : "JSON",                    //Se define del tipo JSON ya que recibiremos datos
                                                    //Solo se envían datos, por eso no usamos el parámetro "Data:"
                        success: function(data)  {
                               $('#Arut_edit').val(data.rut);
                               $('#Anombre_edit').val(data.nombre);
                               $('#Aemail_edit').val(data.email); 
                               $('#Aprivilegio_edit').val(data.privilegio);
                               $('#Acontraseña_edit').val(data.contraseña);
                               $('#Aestado_edit').val(data.estado);
  
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
              }
   
              if (clase=="eliminarUsuario"){
                                         
                  var answer = confirm("¿Seguro que deseas deshabilitar?","si");
                 
                if(answer)  //cuando es verdadera, o sea, que desea eliminar el auto
                  {
                  //hacer nada
                   }
                   else {
                   e.preventDefault();                         //Evita que el evento se genere (redireccionar)

                  }
                }
            });
       });
</script>


<script type="text/javascript">
   $(document).ready(function (){
       $("#rut_").focusout(function(e){
       
        var texto= $(this).val();                 //Se obtiene el id de la bitácora
        var url= "<?php echo base_url().'index.php/Principal/validarPK'?>";
   
        var data = $(this).serializeArray();
        data.push({name:'texto',value:texto});
        data.push({name:'columna',value:"rut"});
        data.push({name:'tabla',value:"usuarios"});
        
        $.ajax({
                  url: url,
                  type: "POST",
                  data : data,
                  dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                  if(data.resultado=="existe")
                    {
                      $.alert({
                        title: 'Error!',
                        content: 'La clave primaria ya existe',
                        confirmButton:'Aceptar',
                        icon: 'fa fa-warning',
                        confirmButtonClass: 'btn-danger',
                        theme: 'white',       //Puede ser white (normal)
                        //confirmKeys: [13], // ENTER key
                       // cancelKeys: [27], // ESC key
                        confirm: function(){
                             $("#rut_").val("");
                          }                                               
                        });                      
                        
                    }

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
     });
 });
</script>







<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>




<script type="text/javascript" charset="utf-8"> 
 $(document).ready(function() {
            $('#tabla_dinamica').DataTable({
                  "language": {
                      "lengthMenu": "Display _MENU_ records per page",
                      "zeroRecords": "No se encontró ninguna entrada",
                      "info": "Mostrando _PAGE_ de _PAGES_",
                      "infoEmpty": "No hay registros disponibles",
                      "infoFiltered": "(Filtrado de los registros totales _MAX_ )"
                   }
              });
              $("div#tabla_dinamica_length").empty();
              $("div#tabla_dinamica_paginate").empty();
              $("div#tabla_dinamica_info").empty();
              $("div#tabla_dinamica_wrapper").removeClass();
              $("div#tabla_dinamica_wrapper").addClass("form-group");
              $("div#tabla_dinamica_filter").find("input").addClass("form-control");
              $("div#tabla_dinamica_filter").removeClass();
              $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
              //$("div#tabla_dinamica_filter").find("label").remove();
             // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
              $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
              $(".dataTables_paginate").empty();
              $(".paging_simple_numbers").empty();
             
          });
</script> 
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>
