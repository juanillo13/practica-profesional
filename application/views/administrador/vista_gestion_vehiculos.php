<div class="container">
<div class="row ">
    <div class="col-md-3">    
       <div class="row">
                        
          <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAgregarAuto">
                  <i class="glyphicon glyphicon-plus"></i> 
                    Vehículo
          </button> 
                        
     </div>
  </div>
 <div>

<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Vehículos</h3> 
                   </div>
                   
         
          <!-- tabla que muestra los datos del vehiculo-->
              <table id="tabla_dinamica" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-responsive"   >
                  <thead> 
                    <tr>
                              <th>Patente</th>
                              <th>Descripción</th>
                              <th>Tipo</th>
                              <th>Año</th>
                              <th>Color</th>
                              <th>Responsable</th> <!-- nota: No es necesario mostrar el estado en la tabla-->
                              <th>Opciones</th>
                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?> <!-- recorre la lista y deja los valores dentro de la tabla-->
                             <tr>
                                                                
                                 <td> <?=$row->patente?> </td> 
                                 <td> <?=$row->descripcion?> </td>
                                 <td> <?=$row->tipo?> </td>
                                 <td> <?=$row->año?> </td>
                                 <td> <?=$row->color?> </td>
                                 <td> <?=$row->responsable?> </td>
                              
                                  <td class="col-lg-4"> <!--Botones para acciones de editar y eliminar, van dentro de la tabla-->

                                 
                                    <a href="<?=$row->patente?>"class="editarVehiculo"><!--le he borrado un class respecto a lo del jota, revisar en caso de cuaquier cosa-->
                                      <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalEditar">
                                              <i class="glyphicon glyphicon-edit"></i> 
                                        Editar
                                      </button>
                                    </a>

                                    <a href="<?php echo base_url().'index.php/administrador/Controlador_Vehiculos'?>/eliminarVehiculo/<?=$row->patente?>" class="eliminarVehiculo"> <!--tengo que crear la funcion eliminar-->
                                    <button  type="button" class="btn btn-danger" data-toggle="modal"><i class="glyphicon glyphicon-trash"></i> 
                                     Eliminar
                                    </button>
                                    </a>
                                   
                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
      
      </div>
   </div>
</div>
</div>

 <!--********************************* MODAL EDITAR VEHICULO ***************************************-->

<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Editar vehiculo</h4>
          </div>
          <div class="modal-body">

               <form  id="formEditar" action ="../Controlador_Vehiculos/editarVehiculo" method="POST"> <!-- en el acction iba EditarAlumno en el formualario del jota-->
                      <div>
                           <div align="center" > 
                            Patente:
                            <input id="Apatente_edit" type="text" name="patente_" class="form-control" readonly />                                                            
                            Descripcion:
                            <input id="Adescripcion_edit" type="Text" name="descripcion_" class="form-control" required />
                            Tipo:
                            <select id ="Atipo_edit" name="tipo_" class="form-control"> 
                            <option value="Automóvil">Automóvil</option> 
                            <option value="Camioneta">Camioneta</option>  
                            <option value="Camión">Camión</option>
                            <option value="Bus">Bus</option>  
                            </select> 
                            Año:
                            <input id="Aanio_edit" type="Text" name="anio_" class="form-control" required />
                            Color:
                            <input id="Acolor_edit" type="Text" name="color_" class="form-control" required />
                            Responsable:
                            <input id="Aresponsable_edit" type="Text" name="responsable_" class="form-control" required />
          
                            Estado:
                            <select id= "Aestado_edit"name="Aestado_edit" class="form-control"> 
                                <option value="0">Habilitado</option> 
                                <option value="1">Deshabilitado</option>  
                            </select> 

                            <br />
                            <br />
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                      </div>
                        </div>
                  </div>
              </form>
          </div>
      </div>
    </div>
</div>






 <!--********************************* MODAL AGREGAR VEHICULO ***************************************-->
<div class="modal fade" id="ModalAgregarAuto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4  align="center" class="modal-title" id="myModalLabel">Ingreso Nuevo Vehiculo</h4>
          </div>
          <div class="modal-body">

               <form id="FormIngresarAuto" action ="../Controlador_Vehiculos/agregarVehiculo" method="POST">  <!-- le agrege el action al formulario-->
                 <div align="center" > 
                
                      Patente: 
                      <input id ="patente_" name="patente_" type="text"  class="form-control" required />                                                            
                      Descripción:
                      <input id ="descripcion_" name="descripcion_" type="text"  class="form-control" required  /> 
                     Tipo: 
                      <select id = "tipo_" name="tipo_" class="form-control"> 
                      <option value="Automóvil">Automóvil</option> 
                      <option value="Camioneta">Camioneta</option>  
                      <option value="Camión">Camión</option>
                      <option value="Bus">Bus</option>  
                      </select> 
                      Año: 
                      <input id="anio_" name= "anio_" type="number"  class="form-control" required />
                      Color:
                      <input  id="color_" name="color_" type="Text" class="form-control"  />
                      Responsable
                      <input  id ="responsable_" name ="responsable_" type="Text" class="form-control"  />
                      Estado:
                      <select id ="estado_" name="estado_" class="form-control"> 
                      <option value="0">Habilitado</option> 
                      <option value="1">Deshabilitado</option>  
                      </select> 
                      <br />
                      <br />
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-success">Guardar cambios</button>            <!--Se elimina el valor SUBMIT ya que Cumpliría el aCtion-->
                </div>
                 
              </form>
          </div>
      </div>
    </div>
</div>


<script type="text/javascript">
       $(document).ready(function (){
           $("a").click(function(e){
                 var clase= $(this).attr("class");

             if (clase =="editarVehiculo") {
                 e.preventDefault();                         //Evita que el evento se genere (redireccionar)
                 var url = "<?php echo base_url().'index.php/administrador/Controlador_Vehiculos'?>"+"/buscarVehiculo/"; 
                 var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
             
                $.ajax({
                        url: url+link,
                        type: "POST",
                        //data : data,
                        dataType : 'JSON',                    //Se define del tipo JSON ya que recibiremos datos
                                                    //Solo se envían datos, por eso no usamos el parámetro "Data:"
                        success: function(data)  {
                               $('#Apatente_edit').val(data.patente);
                               $('#Adescripcion_edit').val(data.descripcion);
                               $('#Atipo_edit').val(data.tipo);
                               $('#Aanio_edit').val(data.anio);
                               $('#Acolor_edit').val(data.color_);
                               $('#Aresponsable_edit').val(data.responsable);
                               $('#Aestado_edit').val(data.estado);
  
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
              }
   
              if (clase=="eliminarVehiculo"){
                  var answer = confirm("¿Seguro que deseas deshabilitar?","si");
                 
                if(answer)  //cuando es verdadera, o sea, que desea eliminar el auto
                  {
        
                   }
                  else {
                   e.preventDefault();                         //Evita que el evento se genere (redireccionar)

                  }
                }
            });
       });
</script>


<script type="text/javascript" charset="utf-8"> 
 $(document).ready(function() {
            $('#tabla_dinamica').DataTable({
                  "language": {
                      "lengthMenu": "Display _MENU_ records per page",
                      "zeroRecords": "No se encontró ninguna entrada",
                      "info": "Mostrando _PAGE_ de _PAGES_",
                      "infoEmpty": "No hay registros disponibles",
                      "infoFiltered": "(Filtrado de los registros totales _MAX_ )"
                   }
              });
              $("div#tabla_dinamica_length").empty();
              $("div#tabla_dinamica_paginate").empty();
              $("div#tabla_dinamica_info").empty();
              $("div#tabla_dinamica_wrapper").removeClass();
              $("div#tabla_dinamica_wrapper").addClass("form-group");
              $("div#tabla_dinamica_filter").find("input").addClass("form-control");
              $("div#tabla_dinamica_filter").removeClass();
              $("div#tabla_dinamica_filter").addClass("col-xs-offset-9 form-group");
              //$("div#tabla_dinamica_filter").find("label").remove();
             // $("div#tabla_dinamica_filter").append('<label>Buscar:<input type="search" class="form-control" placeholder="" aria-controls="tabla_dinamica"></label>');
              $("div#tabla_dinamica_filter").find("label").addClass("control-label col-xs-12 cambiar-search");
              $(".dataTables_paginate").empty();
              $(".paging_simple_numbers").empty();
             
          });
</script> 
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>


<script type="text/javascript">
   $(document).ready(function (){
       $("#patente_").focusout(function(e){
        //alert("asda");
        var texto= $(this).val();                 //Se obtiene el id de la bitácora
        var url= "<?php echo base_url().'index.php/Principal/validarPK'?>";
   
        var data = $(this).serializeArray();
        data.push({name:'texto',value:texto});    //Valor del campo de texto
        data.push({name:'columna',value:"patente"});
        data.push({name:'tabla',value:"vehiculos"});

        $.ajax({
                  url: url,
                  type: "POST",
                  data : data,
                  dataType : "JSON",                   //Se define del tipo JSON ya que recibiremos datos
                                                       //Solo se envían datos, por eso no usamos el parámetro "Data:"
                  success: function(data)  {
                  if(data.resultado=="existe")
                    {
                        $.alert({
                        title: 'Error!',
                        content: 'La clave primaria ya existe',
                        confirmButton:'Aceptar',
                        icon: 'fa fa-warning',
                        confirmButtonClass: 'btn-danger',
                        theme: 'white',       //Puede ser white (normal)
                        //confirmKeys: [13], // ENTER key
                       // cancelKeys: [27], // ESC key
                        confirm: function(){
                             $("#patente_").val("");
                          }                                               
                        });                      
                        
                    }

                  },
                  error: function(result) {
                  console.log("Error" + result);
                  }
          });
     });
 });
</script>