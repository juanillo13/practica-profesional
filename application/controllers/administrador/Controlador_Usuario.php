<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_Usuario extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
      if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
        switch ($this->session->userdata("privilegio")) {
              case '1':
                redirect("digitador/Controlador_digitador");
                break;
              case '2':
                redirect("digitador/Controlador_digitador2");
                break;
              default:
                redirect('Principal');
                break;
        }
    }
     $this->load->model('Modelo_usuario'); //cargamos modelo
  
  }
  
  public function agregarUsuario() {

     
      //Se puede hacer sin ajax también, solo con apretar el button del type ="submit" en el formulario y cambiarle la forma de obtener la info.
      $dataUsuario = array(
          "rut" =>$this->input->post('rut_'),
          "nombre" =>$this->input->post('nombre_'),
          "email" =>$this->input->post('email_'),
          "privilegio" =>$this->input->post('privilegio_'),
          "contraseña"=>$this->input->post('contraseña_'),
          "estado"=>$this->input->post('estado_'),
      );
      
      $this->Modelo_usuario->crearUsuario($dataUsuario);
      redirect("Administrador/Controlador_administrador/MantenedorUsuarios"); //redirecciona al mantenedor 
 
}
    


                                                /*EDITAR VEHICULO*/
   
    
  public function editarUsuario(){


            $codigo = $this->input->post('rut_'); //
            $usuario = array(
                //la parte de la izq son los atributos en la bd y la derecha es la info que viene del formulario
                "nombre" =>$this->input->post('nombre_'),
                "email" =>$this->input->post('email_'), 
                "privilegio" =>$this->input->post('privilegio_'),   
                "contraseña" =>$this->input->post('contraseña_'),
                "estado" =>$this->input->post('estado_'),
                );

            $this->Modelo_usuario->editar_usuario($codigo,$usuario);
         redirect("Administrador/Controlador_administrador/MantenedorUsuarios"); //me redirecciona al mantenedor 
  }

  public function eliminarUsuario($rut){

             $usuario = array(
                'estado' => 1,
             );
            //$patente =  $this->input->post('patente');

             //$this->load->model('Modelo_vehiculo');
             $this->Modelo_usuario->eliminar_usuario($rut,$usuario);
             redirect("Administrador/Controlador_administrador/MantenedorUsuarios"); //me redirecciona al mantenedor 

    }




    
  public function buscarUsuario($rut){

     
                $query =$this->Modelo_usuario->getUsuario($rut);//este es el resultado -> algo que muestra en el formulario
                $data = array();

                    foreach ($query as $row) {

                        $data['rut']=$row->rut;
                        $data['nombre'] = $row->nombre;
                        $data['email'] = $row->email;
                        $data['privilegio'] = $row->privilegio;
                        $data['contraseña'] = $row->contraseña;
                        $data['estado'] = $row->estado;
                    }
                    echo json_encode($data); //enviamos el array por json
       } //fin funcion
   

public function verUsuariosDeshabilitados(){ // carga el mantenedor de bitacoras del digitador

    $data['resultado'] = $this->Modelo_usuario->getusuariosdeshabilitados(); 
    $this->load->view('menu/headerAdministrador');
    $this->load->view('Administrador/vista_lista_usuarios_deshabilitados',$data);
  }
public function habilitarUsuario($rut){ // carga el mantenedor de bitacoras del digitador
      
    $usuario = array(
                'estado' => 0,
             );
      $data['resultado'] = $this->Modelo_usuario->getusuariosdeshabilitados(); 
      $this->Modelo_usuario->habilitar_usuario($rut,$usuario);
            
      redirect('Administrador/Controlador_Usuario/verUsuariosDeshabilitados');
}


   }//FIN 


