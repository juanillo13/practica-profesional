<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_Reservas extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
        switch ($this->session->userdata("privilegio")) {
              case '1':
                redirect("digitador/Controlador_digitador");
                break;
              case '2':
                redirect("digitador/Controlador_digitador2");
                break;
              default:
                redirect('Principal');
                break;
        }
    }
    
  	$this->load->model('Modelo_reserva');
  }
	



	
  public function eliminarReserva($id_reserva){

             $reserva = array(
                'estado' => 1,
             );
            
             $this->Modelo_reserva->eliminar_reserva($id_reserva,$reserva);
             redirect("Administrador/Controlador_administrador/MantenedorReservas"); //me redirecciona al mantenedor 

    }

    public function eliminarReservasfisica($id_reserva){

             $this->Modelo_reserva->eliminar_reservafisica($id_reserva);
             redirect("Administrador/Controlador_administrador/MantenedorReservas"); //me redirecciona al mantenedor 

    }

public function detalleReserva($id_reserva)
{
  $query= $this->Modelo_reserva->getReserva($id_reserva);
  $data = array();

    foreach ($query as $row) {

        $data['id_agenda']=$row->id_agenda;
        $data['rut_conductor'] = $row->rut_conductor;
        $data['nombre_conductor'] = $row->nombre_conductor;
        $data['vehiculo_patente'] = $row->vehiculo_patente;
        $data['fecha'] = $row->fecha;
        $data['horario'] = $row->horario;
        $data['rut_usuario'] = $row->rut_usuario;
        $data['destino'] = $row->destino;
        $data['observaciones'] = $row->observaciones;
    }
    echo json_encode($data);

}


    public function verReservasDeshabilitadas(){ 

    $data['resultado'] = $this->Modelo_reserva->getreservasdeshabilitadas(); 
    $this->load->view('menu/headerAdministrador');
    $this->load->view('Administrador/vista_lista_reservas_deshabilitadas',$data);
  }
public function habilitarReserva($id_agenda){ 

    $reserva = array(
                'estado' => 0,
             );
      $data['resultado'] = $this->Modelo_reserva->getreservasdeshabilitadas(); 
      $this->Modelo_reserva->habilitar_reserva($id_agenda,$reserva);
            
      redirect('Administrador/Controlador_Reservas/verReservasDeshabilitadas');
}

       
}//Fin del controlador_Bitacora