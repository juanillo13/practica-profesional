<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Reporte_vehiculos extends CI_Controller {
 
    public function index()
    {
        ob_end_clean();
        // Se carga el modelo alumno
        $this->load->model('Modelo_vehiculo');
        // Se carga la libreria fpdf
        $this->load->library('Pdf');
 
        // Se obtienen los alumnos de la base de datos
        $query = $this->Modelo_vehiculo->getVehiculos();
 
        // Creacion del PDF
 
        /*
         * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
         * heredó todos las variables y métodos de fpdf
         */

        $this->pdf = new Pdf();


        // Agregamos una página
        $this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
 
        /* Se define el titulo, márgenes izquierdo, derecho y
         * el color de relleno predeterminado
         */
        

        $this->pdf->SetTitle("Lista de Vehiculos");
        $this->pdf->SetLeftMargin(15);
        $this->pdf->SetRightMargin(5);
        $this->pdf->SetFillColor(200,200,200);
 
        // Se define el formato de fuente: Arial, negritas, tamaño 9
        $this->pdf->SetFont('Arial', 'B', 15);
        /*
         * TITULOS DE COLUMNAS
         *
         * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
         */

        $this->pdf->Cell(180,10,utf8_decode("Lista de Vehículos Municipales"),150,20,'C');

        $this->pdf->Ln(20);

        
        $this->pdf->SetFont('Arial', 'B', 11);

        $this->pdf->Cell(17,10,'Patente','TBL',0,'L','0');
        $this->pdf->Cell(60,10,utf8_decode("Descripción"),'TB',0,'L','0');
        $this->pdf->Cell(27,10,'Tipo','TB',0,'L','0');
        $this->pdf->Cell(30,10,utf8_decode("Año"),'TB',0,'L','0');
        $this->pdf->Cell(25,10,'Color','TB',0,'L','0');
        $this->pdf->Cell(30,10,'Responsable','TBR',0,'L','0');
        //$this->pdf->Cell(20,7,'Costo','TBR',0,'L','1');
        //$this->pdf->Cell(10,7,'Estado','TBR',0,'L','1');
        $this->pdf->Ln(10 );
        
        $this->pdf->SetFont('Arial','', 10);
        foreach ($query as $resultado) {
           
            // Se imprimen los datos de cada actividad
            $this->pdf->Cell(17,10,utf8_decode($resultado->patente),'BL',0,'L',0);
            $this->pdf->Cell(60,10,utf8_decode($resultado->descripcion),'B',0,'L',0);
            $this->pdf->Cell(27 ,10,utf8_decode($resultado->tipo),'B',0,'L',0);
            $this->pdf->Cell(30,10,utf8_decode($resultado->año),'B',0,'L',0);
            $this->pdf->Cell(25,10,utf8_decode($resultado->color),'B',0,'L',0);
            $this->pdf->Cell(30,10,utf8_decode($resultado->responsable),'BR',0,'L',0);
           

            //Se agrega un salto de linea
            $this->pdf->Ln(10);
        }
        /*
         * Se manda el pdf al navegador
         *
         * $this->pdf->Output(nombredelarchivo, destino);
         *
         * I = Muestra el pdf en el navegador
         * D = Envia el pdf para descarga
         *
         */
        $this->pdf->Output("Lista de Vehículos.pdf", 'I');
    }
}
