<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_Vehiculos extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
      if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
        switch ($this->session->userdata("privilegio")) {
              case '1':
                redirect("digitador/Controlador_digitador");
                break;
              case '2':
                redirect("digitador/Controlador_digitador2");
                break;
              default:
                redirect('Principal');
                break;
        }
    }
     $this->load->model('Modelo_vehiculo');
  
  }

  
  public function agregarVehiculo() {

     
      //Se puede hacer sin ajax también, solo con apretar el button del type ="submit" en el formulario y cambiarle la forma de obtener la info.
       $dataVehiculo = array(
                //la parte de la izq son los atributos en la bd y la derecha es la info que viene del formulario
                "patente" =>$this->input->post('patente_'),
                "descripcion" =>$this->input->post('descripcion_'), 
                "tipo" =>$this->input->post('tipo_'),
                "año" =>$this->input->post('anio_'),
                "color" =>$this->input->post('color_'),
                "responsable" =>$this->input->post('responsable_'),
                "estado" =>$this->input->post('estado_'),
       );
  
      $this->Modelo_vehiculo->crearVehiculo($dataVehiculo);
      
      redirect("Administrador/Controlador_administrador/MantenedorVehiculos"); //me redirecciona al mantenedor 
    
}
    
                                             /*EDITAR VEHICULO*/
   public function editarVehiculo(){

  
            $codigo = $this->input->post('patente_'); //
            $Vehiculo = array(
                //la parte de la izq son los atributos en la bd y la derecha es la info que viene del formulario
                "descripcion" =>$this->input->post('descripcion_'), 
                "tipo" =>$this->input->post('tipo_'),
                "año" =>$this->input->post('anio_'),
                "color" =>$this->input->post('color_'),
                "responsable" =>$this->input->post('responsable_'),
                "estado" =>$this->input->post('estado_'),
                );

         $this->Modelo_vehiculo->editar_vehiculo($codigo,$Vehiculo);
         redirect("Administrador/Controlador_administrador/MantenedorVehiculos"); //me redirecciona al mantenedor 

    }




  

  public function eliminarVehiculo($patente){

             $Vehiculo = array(
              'estado' => 1,
             );
            //$patente =  $this->input->post('patente');
             $this->Modelo_vehiculo->eliminar_vehiculo($patente,$Vehiculo);
             redirect("Administrador/Controlador_administrador/MantenedorVehiculos"); //me redirecciona al mantenedor 

    }




    
  public function buscarVehiculo($patente) {

       $query =$this->Modelo_vehiculo->getVehiculo($patente);//este es el resultado -> algo que muestra en el formulario
                $data = array();

                    foreach ($query as $row) {

                        $data['patente']=$row->patente;
                        $data['descripcion'] = $row->descripcion;
                        $data['tipo'] = $row->tipo;
                        $data['anio'] = $row->año;
                        $data['color_'] = $row->color;
                        $data['responsable'] = $row->responsable;
                        $data['estado'] = $row->estado;
                    }
                    echo json_encode($data); //no se para que es esto ><
                 
       } //fin funcion
   

public function verVehiculosDeshabilitados(){ // carga el mantenedor de bitacoras del digitador


    $data['resultado'] = $this->Modelo_vehiculo->getvehiculosdeshabilitados(); 
    $this->load->view('menu/headerAdministrador');
    $this->load->view('Administrador/vista_lista_vehiculos_deshabilitados',$data);

  }
public function habilitarVehiculo($patente){ // carga el mantenedor de bitacoras del digitador

     
    $vehiculo = array(
                'estado' => 0,
             );
      $data['resultado'] = $this->Modelo_vehiculo->getvehiculosdeshabilitados(); 
      $this->Modelo_vehiculo->habilitar_vehiculo($patente,$vehiculo);
            
      redirect('Administrador/Controlador_Vehiculos/verVehiculosDeshabilitados');
}


    }//FIN 



	


	
