<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_Bitacoras extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    /*SE INTENTA DE ENTRAR DE FORMA INDEBIDA  AL SISTEMA */
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
    switch ($this->session->userdata("privilegio")) {
          case '1':
            redirect("digitador/Controlador_digitador");
            break;
          case '2':
            redirect("digitador/Controlador_digitador2");
            break;
          default:
            redirect('Principal');
            break;
    }
  }
  	$this->load->model('Modelo_bitacora');
  }
	


  public function agregarBitacora() {
  
    
   
    
  
    $n_bitacora = $this->input->post('n_bitacora');
    $vehiculo_patente = $this->input->post('patente_vehiculo');
    $rut_conductor = $this->input->post('rut_conductor');
    $fecha = $this->input->post('fecha');
    $destino = $this->input->post('destino');
    $carga_Combustible = $this->input->post('combustible');
    $km_salida = $this->input->post('kmsalida');
    $km_llegada = $this->input->post('kmllegada');
    $km_Recorrido = $this->input->post('kmrecorridos');
    $estado = $this->input->post('estado');
    $rut_usuario = $this->input->post('rutdigitador');
    $observaciones = $this->input->post('observaciones');



    $this->load->model('Modelo_bitacora');//carga el modelo asociado 
    $this->ModeloActividad->crearbitacora($n_bitacora, $vehiculo_patente,$rut_conductor,$fecha,$destino,$carga_Combustible,$km_salida,$km_llegada,$km_Recorrido,$estado,$rut_usuario,$observaciones);
    redirect('Controlador_digitador/cargarFormularioBitacora');

  
}

	
  public function eliminarBitacora($n_bitacora){

             $bitacora = array(
                'estado' => 1,
             );
            //$patente =  $this->input->post('patente');

             //$this->load->model('Modelo_vehiculo');
             $this->Modelo_bitacora->eliminar_bitacora($n_bitacora,$bitacora);
             redirect("Administrador/Controlador_administrador/MantenedorBitacoras"); //me redirecciona al mantenedor 

    }

    public function eliminarBitacorafisica($n_bitacora){

             $this->Modelo_bitacora->eliminar_bitacorafisica($n_bitacora);
             redirect("Administrador/Controlador_administrador/MantenedorBitacoras"); //me redirecciona al mantenedor 

    }




    
  public function buscarBitacora($n_bitacora){

    
            if (($this->session->userdata('logged_in'))==TRUE)
           {      
                $query =$this->Modelo_bitacora->getbitacora($n_bitacora);//este es el resultado -> algo que muestra en el formulario
                $data = array();
            
                    foreach ($query as $row) {

                        $data['n_bitacora']=$row->n_bitacora;
                        $data['nombre_conductor'] = $row->nombre_conductor;
                        $data['rut_conductor'] = $row->rut_conductor;
                        $data['rut_usuario'] = $row->rut_usuario;
                        $data['vehiculo_patente'] = $row->vehiculo_patente;
                        $data['km_salida'] = $row->km_salida;
                        $data['km_llegada'] = $row->km_llegada;
                        $data['km_Recorridos'] = $row->km_Recorridos;
                        $data['fecha'] = $row->fecha;
                        $data['destino'] = $row->destino;
                        $data['carga_Combustible'] = $row->carga_Combustible;
                        $data['observaciones'] = $row->observaciones;
                                         
                           
                    }
                    echo json_encode($data); 

             }//fin del if 
                 
       } //fin funcion
   


    //método para mostrar las bitacoras mensuales de un vehículo 

       public function bitacorasMensuales(){
   
            $patente =  $_POST['patente'];
            $mes = $_POST['mes'];
            $anio = $_POST['anio'];
         
            $query= $this->Modelo_bitacora->actividadesMensuales($patente,$mes,$anio);
            $datos = array();
            foreach ($query as $row) {
                        array_push($datos,
                                    array (
                                    'n_bitacora'=>$row->n_bitacora,
                                    'patente' => $row->vehiculo_patente, 
                                    'rut_conductor'=>$row->rut_conductor, 
                                    'nombre_conductor' => $row->nombre_conductor, 
                                    'fecha'=>$row->fecha, 
                                    'destino' => $row->destino,                                                   
                                    )
                        );
                }
           echo json_encode($datos);
         

       }//fin funcion bM

public function verBitacorasDeshabilitadas(){ // carga el mantenedor de bitacoras del digitador
 
    $data['resultado'] = $this->Modelo_bitacora->getbitacorasdeshabilitadas(); 
    $this->load->view('menu/headerAdministrador');
    $this->load->view('Administrador/vista_lista_bitacoras_deshabilitadas',$data);
  }
public function habilitarBitacora($n_bitacora){ 

    $bitacora = array(
                'estado' => 0,
             );
      $data['resultado'] = $this->Modelo_bitacora->getbitacorasdeshabilitadas(); 
      $this->Modelo_bitacora->habilitar_bitacora($n_bitacora,$bitacora);
            
      redirect('Administrador/Controlador_Bitacoras/verBitacorasDeshabilitadas');
}
       
}//Fin del controlador_Bitacora