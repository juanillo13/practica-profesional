<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_administrador extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    /*SE INTENTA DE ENTRAR DE FORMA INDEBIDA  AL SISTEMA */
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
		switch ($this->session->userdata("privilegio")) {
					case '1':
						redirect("digitador/Controlador_digitador");
						break;
					case '2':
						redirect("digitador/Controlador_digitador2");
						break;
					default:
						redirect('Principal');
						break;
		}
	}
    $this->load->model('Modelo_vehiculo');// cargamos modelo relacionado al vehiculo
  	$this->load->model('Modelo_bitacora');
  	$this->load->model('Modelo_conductor');
  	$this->load->model('Modelo_usuario');
  	$this->load->model('Modelo_reserva');

  }

//*******ESTE CONTROLADOR VA A SER EL ENCARGADO DE CARGAR LAS VISTAS********//
	

	public function index()
	{
	
			$data['resultado'] = $this->Modelo_reserva->getVehiculosDiarios();
			$this->load->view('menu/headerAdministrador');
			$this->load->view('administrador/vista_administrador',$data);

	}

	public function MantenedorVehiculos(){ //aca debo pasar la informacion de los vehiculos para cargar las tablas

			
		$data['resultado'] = $this->Modelo_vehiculo->getVehiculos();
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_gestion_vehiculos',$data);

	}

	public function MantenedorConductores(){


			
		$data['resultado'] = $this->Modelo_conductor->getConductores();
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_gestion_conductores',$data);

	}

	public function MantenedorBitacoras(){ 


		$data['resultado'] = $this->Modelo_bitacora->getBitacoras(); 
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_lista_bitacora',$data);


	}


	public function MantenedorReservas(){ // carga el mantenedor de Reservas del digitador

			
		$data['resultado'] = $this->Modelo_reserva->getreservas(); 
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_lista_reserva',$data);

	}

	public function MantenedorUsuarios(){ 


			
		$data['resultado'] = $this->Modelo_usuario->getUsuarios(); 
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_gestion_usuarios',$data);
	}





public function Reportes(){


			
			$query["resultado"]= $this->Modelo_vehiculo->getVehiculos();
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_reportes',$query);
		}


public function cargarBuscarVehiculo(){

			
		$query["resultado"]= $this->Modelo_vehiculo->getVehiculos();
		$this->load->view('menu/headerAdministrador');
		$this->load->view('Administrador/vista_buscarvehiculo',$query);
}

public function descargarArchivo($id_bitacora)
	{
		
		$file = $this->Modelo_bitacora->getArchivo($id_bitacora);

		foreach ($file as $row) {
				//$file2 = implode("",$row->archivo);
				header("Content-disposition: attachment; filename=".$row->nombre_archivo);
				header("Content-type:".$row->tipo_archivo);
				print($row->archivo);
       
    	}
		

	}




}