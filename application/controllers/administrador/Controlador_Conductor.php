<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_Conductor extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    /*SE INTENTA DE ENTRAR DE FORMA INDEBIDA  AL SISTEMA */
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='0'){ 
        switch ($this->session->userdata("privilegio")) {
              case '1':
                redirect("digitador/Controlador_digitador");
                break;
              case '2':
                redirect("digitador/Controlador_digitador2");
                break;
              default:
                redirect('Principal');
                break;
        }
    }
     $this->load->model('Modelo_conductor'); //cargamos modelo
  
  }



  


  
  public function agregarConductor() {

     
      //Se puede hacer sin ajax también, solo con apretar el button del type ="submit" en el formulario y cambiarle la forma de obtener la info.
      $dataConductor = array(
          "rut" =>$this->input->post('rut_'),
          "nombre_conductor" =>$this->input->post('nombre_'),
          "telefono" =>$this->input->post('telefono_'),
          "email" =>$this->input->post('email_'),
          "categoria_patente"=>$this->input->post('categoria_'),
          "estado"=>$this->input->post('estado_'),
      );
      
      $this->Modelo_conductor->crearConductor($dataConductor);
      redirect("Administrador/Controlador_administrador/MantenedorConductores"); //redirecciona al mantenedor 
}
    


                                                /*EDITAR CONDUCTOR*/
   
    

    
  public function editarConductor(){


           $codigo = $this->input->post('rut_'); //
            $conductor = array(
                //la parte de la izq son los atributos en la bd y la derecha es la info que viene del formulario
                "nombre_conductor" =>$this->input->post('nombre_'), 
                "telefono" =>$this->input->post('telefono_'),
                "email" =>$this->input->post('email_'),
                "categoria_patente" =>$this->input->post('categoria_'),
                "estado" =>$this->input->post('estado_'),
                );

            $this->Modelo_conductor->editar_conductor($codigo,$conductor);
         redirect("Administrador/Controlador_administrador/MantenedorConductores"); //me redirecciona al mantenedor 
    }




  

  public function eliminarConductor($rut){

             $conductor = array(
                'estado' => 1,
             );
            //$patente =  $this->input->post('patente');

             //$this->load->model('Modelo_vehiculo');
             $this->Modelo_conductor->eliminar_conductor($rut,$conductor);
             redirect("Administrador/Controlador_administrador/MantenedorConductores"); //me redirecciona al mantenedor 

    }




    
  public function buscarConductor($rut){

    
    
                $query =$this->Modelo_conductor->getConductor($rut);//este es el resultado -> algo que muestra en el formulario
                $data = array();

                    foreach ($query as $row) {

                        $data['rut']=$row->rut;
                        $data['nombre_conductor'] = $row->nombre_conductor;
                        $data['telefono'] = $row->telefono;
                        $data['email'] = $row->email;
                        $data['categoria_patente'] = $row->categoria_patente;
                        $data['estado'] = $row->estado;
                    }
                    echo json_encode($data); //no se para que es esto ><
       } //fin funcion
   


public function verConductoresDeshabilitados(){ // carga el mantenedor de bitacoras del digitador

    $data['resultado'] = $this->Modelo_conductor->getconductoresdeshabilitados(); 
    $this->load->view('menu/headerAdministrador');
    $this->load->view('Administrador/vista_lista_conductores_deshabilitados',$data);

  }
public function habilitarConductor($rut){ // carga el mantenedor de bitacoras del digitador


    $conductor = array(
                'estado' => 0,
             );
      $data['resultado'] = $this->Modelo_conductor->getconductoresdeshabilitados(); 
      $this->Modelo_conductor->habilitar_conductor($rut,$conductor);
            
      redirect('Administrador/Controlador_Conductor/verConductoresDeshabilitados');
}
   }//FIN 


