<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_digitador extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre

    parent::__construct();
    $this->user = $this->session->userdata("rut");

    /*SE INTENTA DE ENTRAR DE FORMA INDEBIDA  AL SISTEMA */
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='1'){ 
		switch ($this->session->userdata("privilegio")) {
					case '0':
						redirect("administrador/Controlador_administrador");
						break;
					case '2':
						redirect("digitador/Controlador_digitador2");
						break;
					default:
						redirect('Principal');
						break;
		}
	}
   // $this->load->model('Modelo_vehiculo');// cargamos modelo relacionado al vehiculo
  	$this->load->model('Modelo_bitacora');
  	$this->load->model('Modelo_reserva');
  }

//*******ESTE CONTROLADOR VA A SER EL ENCARGADO DE CARGAR LAS VISTAS********//
	

	public function index(){
		$this->load->view('menu/headerDigitador');
	}

	public function verBitacoras(){ // carga el mantenedor de bitacoras del digitador

	
		$data['resultado'] = $this->Modelo_bitacora->getBitacoras(); 
		$this->load->view('menu/headerDigitador');
		$this->load->view('Digitador/vista_lista_bitacoras_digitador',$data);


	}

	public function verBitacorasDeshabilitadas(){ // carga el mantenedor de bitacoras del digitador


			
		$data['resultado'] = $this->Modelo_bitacora->getbitacorasdeshabilitadas(); 
		$this->load->view('menu/headerDigitador');
		$this->load->view('Digitador/vista_lista_bitacoras_digitador_deshabilitadas',$data);


	}
	public function verReservasDeshabilitadas(){ // carga el mantenedor de bitacoras del digitador


		$data['resultado'] = $this->Modelo_reserva->getreservasdeshabilitadas(); 
		$this->load->view('menu/headerDigitador');
		$this->load->view('Digitador/vista_lista_reservas_digitador_deshabilitadas',$data);


	}


	public function habilitarBitacora($n_bitacora){ // carga el mantenedor de bitacoras del digitador


			
		$bitacora = array(
                'estado' => 0,
             );
			$data['resultado'] = $this->Modelo_bitacora->getbitacorasdeshabilitadas(); 
			$this->Modelo_bitacora->habilitar_bitacora($n_bitacora,$bitacora);
            
			redirect('Digitador/Controlador_digitador/verBitacorasDeshabilitadas');
	
}


public function habilitarReserva($id_agenda){ // carga el mantenedor de bitacoras del digitador


		$reserva = array(
                'estado' => 0,
             );
			$data['resultado'] = $this->Modelo_reserva->getreservasdeshabilitadas(); 
			$this->Modelo_reserva->habilitar_reserva($id_agenda,$reserva);
            
			redirect('Digitador/Controlador_digitador/verReservasDeshabilitadas');

}




	public function cargarFormularioBitacora(){ //formulario para agregar bitacora


			$rut_digitador["rut"] = $this->session->userdata("rut");//$this->user
			$this->load->view('menu/headerDigitador');
			$this->load->view('Digitador/vista_formulario_bitacoras',$rut_digitador);	


	}


	public function cargarFormularioBitacora_editar($id_bitacora)
	{

			$data['resultado'] = $this->Modelo_bitacora->getBitacora($id_bitacora); 
			$this->load->view('menu/headerDigitador');
			$this->load->view('Digitador/vista_formulario_bitacoras_editar',$data);



	}
	public function editarBitacora()
	{

	 	$n_bitacora= $this->input->post('n_bitacora');

		$dataEditarBitacora = array(
		"nombre_conductor"=>$this->input->post('rut_conductor'),
		"rut_conductor" => $this->input->post('nombre_conductor'),
		"rut_usuario" => $this->input->post('rutdigitador'),
	    "vehiculo_patente" => $this->input->post('patente_vehiculo'),
	    "km_salida" => $this->input->post('kmsalida'),
	    "km_llegada" => $this->input->post('kmllegada'),
	    "km_Recorridos" => $this->input->post('kmrecorridos'),
	    "fecha" => $this->input->post('fecha'),
	    "destino" => $this->input->post('destino'),
	    "carga_Combustible" => $this->input->post('combustible'),
	    "observaciones" => $this->input->post('observaciones'),	   
	    "estado" => $this->input->post('estado'),
	    );
	    $this->Modelo_bitacora->editarBitacora($n_bitacora,$dataEditarBitacora);
        redirect('Digitador/Controlador_digitador/verBitacoras');
	}

	public function agregarBitacora() {
  
     $dataBitacora = array(
	    			"n_bitacora"=>$_POST['n_bitacora'],
					"nombre_conductor" =>$_POST['nombre_conductor'],
					"rut_conductor" =>$_POST['rut_conductor'],
					"rut_usuario" =>$_POST['rut_usuario'],
					"vehiculo_patente" =>$_POST['vehiculo_patente'],
					"km_salida"=>$_POST['km_salida'],
					"km_Recorridos"=>$_POST['km_Recorridos'],
					"km_llegada"=>$_POST['km_llegada'],
					"fecha"=>$_POST['fecha'],
					"destino"=>$_POST['destino'],
					"carga_Combustible"=>$_POST['carga_Combustible'],
					"observaciones"=>$_POST['observaciones'],			
     );
     $this->Modelo_bitacora->crearbitacora($dataBitacora);
    //redirect('Digitador/Controlador_digitador/verBitacoras');
 }




	public function verReservas(){ // carga el mantenedor de Reservas del digitador


			
		$data['resultado'] = $this->Modelo_reserva->getreservas(); 
		$this->load->view('menu/headerDigitador');
		$this->load->view('Digitador/vista_lista_reservas_digitador',$data);


	}


public function cargarFormularioReserva(){ //formulario para agregar bitacora

	
			$rut_digitador["rut"] = $this->session->userdata("rut");//$this->user;
			$this->load->view('menu/headerDigitador');
			$this->load->view('Digitador/vista_formulario_reservas',$rut_digitador);	


	}


public function agregarReserva() {

    $dataReserva = array(
	    			"id_agenda"=>$_POST['id_agenda'],
					"nombre_conductor" =>$_POST['nombre_conductor'],
					"rut_conductor" =>$_POST['rut_conductor'],
					"rut_usuario" =>$_POST['rut_usuario'],
					"vehiculo_patente" =>$_POST['vehiculo_patente'],
					"fecha"=>$_POST['fecha'],
					"destino"=>$_POST['destino'],
					"horario"=>$_POST['horario'],
					"estado"=>$_POST['estado'],
					"observaciones"=>$_POST['observaciones'],
					
     );
  
	 $this->Modelo_reserva->ingresarReserva($dataReserva);
    //redirect('Digitador/Controlador_digitador/verReservas');  
}

public function detalleBitacora($id_bitacora)
{
	$query= $this->Modelo_bitacora->getBitacora($id_bitacora);
	$data = array();

    foreach ($query as $row) {

        $data['n_bitacora']=$row->n_bitacora;
        $data['nombre_conductor'] = $row->nombre_conductor;
        $data['rut_conductor'] = $row->rut_conductor;
        $data['vehiculo'] = $row->vehiculo_patente;
        $data['fecha'] = $row->fecha;
        $data['destino'] = $row->destino;
        $data['km_salida'] = $row->km_salida;
        $data['km_llegada'] = $row->km_llegada;
        $data['km_recorridos'] = $row->km_Recorridos;
        $data['carga_combustible'] = $row->carga_Combustible;
        $data['rut_digitador'] = $row->rut_usuario;
        $data['observaciones'] = $row->observaciones;
    }
    echo json_encode($data);

}

public function detalleReserva($id_reserva)
{
	$query= $this->Modelo_reserva->getReserva($id_reserva);
	$data = array();

    foreach ($query as $row) {

        $data['id_agenda']=$row->id_agenda;
        $data['rut_conductor'] = $row->rut_conductor;
        $data['nombre_conductor'] = $row->nombre_conductor;
        $data['vehiculo_patente'] = $row->vehiculo_patente;
        $data['fecha'] = $row->fecha;
        $data['horario'] = $row->horario;
        $data['rut_usuario'] = $row->rut_usuario;
        $data['destino'] = $row->destino;
        $data['observaciones'] = $row->observaciones;
    }
    echo json_encode($data);

}

public function editarReserva()
	{

	 	$id_agenda= $this->input->post('id_agenda');

		$dataEditarReserva = array(
		"nombre_conductor"=>$this->input->post('rut_conductor'),
		"rut_conductor" => $this->input->post('nombre_conductor'),
		"rut_usuario" => $this->input->post('rutdigitador'),
	    "vehiculo_patente" => $this->input->post('patente_vehiculo'),
		"fecha" => $this->input->post('fecha'),
	    "destino" => $this->input->post('destino'),
	    "rut_usuario" => $this->input->post('rutdigitador'),
	    "observaciones" => $this->input->post('observaciones'),	   
	  //  "estado" => $this->input->post('estado'),
	    );
	    $this->Modelo_reserva->editarReserva($id_agenda,$dataEditarReserva);
        redirect('Digitador/Controlador_digitador/verReservas');
	}
	
	public function cargarFormularioReserva_editar($id_reserva)
	{
		
			$data['resultado'] = $this->Modelo_reserva->getReserva($id_reserva); 
			$this->load->view('menu/headerDigitador');
			$this->load->view('Digitador/vista_formulario_reservas_editar',$data);
			
	}
	public function subirArchivo($id_bitacora)
	{
		
        
		
		//$return = Array('ok'=>TRUE);
		//$upload_folder ='images';
		
		//Algunas caracteristicas
		
		$nombre_archivo = $_FILES['archivo']['name'];
		$tipo_archivo = $_FILES['archivo']['type'];
		$tamano_archivo = $_FILES['archivo']['size'];
		$tmp_archivo = $_FILES['archivo']['tmp_name'];
		
	    $imagenEscapes=file_get_contents($_FILES["archivo"]["tmp_name"]);	//Obtenemos la imagen

		$data = array(
					"archivo" =>$imagenEscapes,	
					"nombre_archivo"=>$nombre_archivo,
					"tipo_archivo"=>$tipo_archivo,		
     			);

		$this->Modelo_bitacora->editarBitacora($id_bitacora,$data);
/*		$archivador = $upload_folder . '/' . $nombre_archivo;

		if (!move_uploaded_file($tmp_archivo, $archivador)) {

		$return = Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 'status' => 'error');

		}*/
		echo json_encode($return);
	
	}

	public function descargarArchivo($id_bitacora)
	{
		
		$file = $this->Modelo_bitacora->getArchivo($id_bitacora);

		foreach ($file as $row) {
				//$file2 = implode("",$row->archivo);
				header("Content-disposition: attachment; filename=".$row->nombre_archivo);
				header("Content-type:".$row->tipo_archivo);
				print($row->archivo);
       
    	}
		

	}


}