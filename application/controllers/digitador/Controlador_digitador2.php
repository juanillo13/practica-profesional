<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador_digitador2 extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
  	/*SE INTENTA DE ENTRAR DE FORMA INDEBIDA  AL SISTEMA */
    if (!$this->session->userdata('logged_in') || $this->session->userdata('privilegio')!='2'){ 
		switch ($this->session->userdata("privilegio")) {
					case '0':
						redirect("administrador/Controlador_administrador");
						break;
					case '1':
						 redirect("digitador/Controlador_digitador");
						break;
					default:
						redirect('Principal');
						break;
		}
	}

  	$this->load->model('Modelo_reserva');
  }

//*******ESTE CONTROLADOR VA A SER EL ENCARGADO DE CARGAR LAS VISTAS********//
	

	public function index()
	{
			$data['resultado'] = $this->Modelo_reserva->getreservas(); 
			$this->load->view('menu/headerDigitador2');
			$this->load->view('digitador/vista_digitador2',$data);
		

	}

	public function cargarFormularioReserva_completar($id_reserva)
	{

			$data['resultado'] = $this->Modelo_reserva->getReserva($id_reserva); 
			$data['digitador'] = $this->session->userdata("rut");
			//$data['conductores'] = $this->Modelo_reserva->getConductores();
			$this->load->view('menu/headerDigitador2');
			$this->load->view('Digitador/vista_formulario_completar_reserva',$data);
			//$this->load->view('Digitador/vista_formulario_completar_reserva',$data2);

	}

	public function completarReserva()
	{
		$id_agenda= $_POST['id_agenda'];

	    $dataEditarReserva = array(
					"nombre_conductor" =>$_POST['nombre_conductor'],
					"rut_conductor" =>$_POST['rut_conductor'],
					"rut_usuario" =>$_POST['rut_usuario'],
					"vehiculo_patente" =>$_POST['vehiculo_patente'],
					"fecha"=>$_POST['fecha'],
					"destino"=>$_POST['destino'],
					"horario"=>$_POST['horario'],
					"observaciones"=>$_POST['observaciones'],
					
     			);

	    $this->Modelo_reserva->editarReserva($id_agenda,$dataEditarReserva);
      //  redirect('digitador/Controlador_digitador2');
	}




}