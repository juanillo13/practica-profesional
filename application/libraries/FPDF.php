<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";
 
    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class FPDF extends FPDF {
        public function __construct() {
            parent::__construct();
        }
        // El encabezado del PDF
        
        public function Header(){
            //$this->Image('imagen/ucm.png',10,8,22);


           $this->SetFont('Arial','',8);
            $this->Cell(0,10,date('d/m/Y'),0,1,'R');

            $this->Image('imagen/logo_muni.png',150,8,22);

            $this->Cell(120,10,'REPUBLICA DE CHILE',0,0,'l');
            $this->Ln('4');
            $this->Cell(120,10,'PROVINCIA DE CAUQUENES',0,0,'l');
            $this->Ln('4');
            $this->Cell(120,10,'ILUSTRE MUNICIPALIDAD DE CAUQUENES',0,0,'l');
            $this->Ln('4');
            $this->Cell(120,10,'DIRECCION DESARROLLO COMUNITARIO',0,0,'l');
            $this->Ln('4');



            $this->Ln('5');
            $this->Cell(0,0,'','',0,'L','1'); //LINEA 
            $this->Ln('10');
            
       }
       // El pie del pdf
       public function Footer(){
           $this->SetY(-15);
           $this->SetFont('Arial','I',8);
           $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
      }
    }
