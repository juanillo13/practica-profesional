<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_usuario extends CI_Model {

  function __construct(){
    parent::__construct();
  }

// insertar vehiculo en la base de datos 
  public function crearUsuario($dataUsuario) {
   /*
    $data = array(
      'patente' => $patente,
      'descripcion' => $descripcion,
      'tipo' => $tipo,
      'año' => $año,
      'color' => $color,
      'responsable' => $responsable,
      'estado' => $estado
    );*/
 
    $this->db->insert('usuarios', $dataUsuario);
  }

public function editar_usuario ($rut,$usuario) {
            
            $this->db->where('rut',$rut);
            $this->db->update('usuarios',$usuario);
          
  }

 



 public function eliminar_usuario ($rut,$usuario) {
            
            $this->db->where('rut',$rut);
            $this->db->update('usuarios',$usuario);
          
  }





//BUSCA TODOS LOS CONDUCTORES QUE ESTEN DISPONIBLES  0 = HABILITADO , 1 = DESHABILITADO
  public function getUsuarios()
    {
    $query = $this->db->query('select * from usuarios where estado="0"')->result();
    return $query;
    }

//BUSCA UN CONDUCTOR POR RUT
      public function getUsuario($rut) {
     
      $query = $this->db->query('select * from usuarios where rut="'.$rut.'"')->result();
      return $query;
  }

public function habilitar_usuario ($rut,$usuario) {
            
            $this->db->where('rut',$rut);
            $this->db->update('usuarios',$usuario);
          
  }

public function getusuariosdeshabilitados()
    {
    $query = $this->db->query('select * from usuarios where estado ="1"')->result();
    return $query;
    }



}