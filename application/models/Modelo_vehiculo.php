<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_vehiculo extends CI_Model {

  function __construct(){
    parent::__construct();
  }

// insertar vehiculo en la base de datos 
  public function crearVehiculo($data) {
   /*
    $data = array(
      'patente' => $patente,
      'descripcion' => $descripcion,
      'tipo' => $tipo,
      'año' => $año,
      'color' => $color,
      'responsable' => $responsable,
      'estado' => $estado
    );*/
 
    $this->db->insert('vehiculos', $data);
   }

public function editar_vehiculo ($patente,$Vehiculo) {
            
            $this->db->where('patente',$patente);
            $this->db->update('vehiculos',$Vehiculo);
          
  }

 

 public function eliminar_vehiculo ($patente,$Vehiculo) {
            
            $this->db->where('patente',$patente);
            $this->db->update('vehiculos',$Vehiculo);
          
  }





//BUSCA TODOS LOS VEHICULOS QUE ESTEN DISPONIBLES 
  public function getVehiculos()
    {
    $query = $this->db->query('select * from vehiculos where estado="0"')->result();
    return $query;
    }

//BUSCA UN VEHICULO POR SU PATENTE
   public function getVehiculo($patente) {
    $query = $this->db->query('select * from vehiculos where patente="'.$patente.'"')->result();
   // $query = $this->db->query('select * from vehiculos where patente="AUTO-2"')->result();

    
    return $query;
  }

public function habilitar_vehiculo ($patente,$vehiculo) {
            
            $this->db->where('patente',$patente);
            $this->db->update('vehiculos',$vehiculo);
          
  }

public function getvehiculosdeshabilitados()
    {
    $query = $this->db->query('select * from vehiculos where estado ="1"')->result();
    return $query;
    }



}