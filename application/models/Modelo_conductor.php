<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_conductor extends CI_Model {

  function __construct(){
    parent::__construct();
  }

// insertar vehiculo en la base de datos 
  public function crearConductor($dataConductor) {
   /*
    $data = array(
      'patente' => $patente,
      'descripcion' => $descripcion,
      'tipo' => $tipo,
      'año' => $año,
      'color' => $color,
      'responsable' => $responsable,
      'estado' => $estado
    );*/
 
    $this->db->insert('conductor', $dataConductor);
  }

public function editar_conductor ($rut,$conductor) {
            
            $this->db->where('rut',$rut);
            $this->db->update('conductor',$conductor);
          
  }

 



 public function eliminar_conductor ($rut,$conductor) {
            
            $this->db->where('rut',$rut);
            $this->db->update('conductor',$conductor);
          
  }





//BUSCA TODOS LOS CONDUCTORES QUE ESTEN DISPONIBLES  0 = HABILITADO , 1 = DESHABILITADO
  public function getConductores()
    {
    $query = $this->db->query('select * from conductor where estado="0"')->result();
    return $query;
    }

//BUSCA UN CONDUCTOR POR RUT
      public function getConductor($rut) {
     
      $query = $this->db->query('select * from conductor where rut="'.$rut.'"')->result();
      return $query;
  }


public function habilitar_conductor ($rut,$conductor) {
            
            $this->db->where('rut',$rut);
            $this->db->update('conductor',$conductor);
          
  }

public function getconductoresdeshabilitados()
    {
    $query = $this->db->query('select * from conductor where estado ="1"')->result();
    return $query;
    }


}