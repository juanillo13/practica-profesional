<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_reserva extends CI_Model {

  function __construct(){
    parent::__construct();
  }



//Agrega reserva por php
public function crearReserva($id_agenda,$vehiculo_patente,$nombre_conductor,$rut_conductor,$fecha,$destino,$horario,$estado,$rut_usuario,$observaciones){

   $data = array(
      'id_agenda' => $id_agenda,
      'rut_conductor' => $rut_conductor,
      'nombre_conductor'=>$nombre_conductor,
      'vehiculo_patente' => $vehiculo_patente,
      'fecha' => $fecha,
      'horario'=>$horario,
      'rut_usuario' => $rut_usuario,
      'destino' => $destino,
      'observaciones' => $observaciones,
      'estado' => $estado
    );
 
    $this->db->insert('agenda_trabajo', $data);


}
//Agrega reserva por ajax
public function ingresarReserva($data){

   $this->db->insert('agenda_trabajo', $data);
}


//BUSCA TODAS LAS RESERVAS QUE ESTEN DISPONIBLES 
  public function getreservas()
    {
    $query = $this->db->query('select * from agenda_trabajo where estado ="0" order by fecha desc')->result();
    return $query;
    }


  public function getReserva($n_reserva) {
     
      $query = $this->db->query('select * from agenda_trabajo where id_agenda="'.$n_reserva.'"')->result();
      return $query;
  }
   public function editarReserva($id_agenda,$dataEditarReserva)
 {
      $this->db->where('id_agenda',$id_agenda);
      $this->db->update('agenda_trabajo',$dataEditarReserva);
 }


public function getVehiculosDiarios()
    {
    $fecha= date('d/m/20y'); 
    $query = $this->db->query('select vehiculo_patente,nombre_conductor,rut_conductor,fecha,destino from agenda_trabajo where fecha ="'.$fecha.'" and estado=0' )->result();
    return $query;
    }
  
public function actividadesMensuales($patente,$mes,$anio) 
{
    $query = $this->db->query('select * from bitacora ');
    return $query;
}


public function eliminar_reserva ($id_agenda,$reserva) {
            
            $this->db->where('id_agenda',$id_agenda);
            $this->db->update('agenda_trabajo',$reserva);
          
  }

  public function eliminar_reservafisica ($id_agenda) {
            
           
           $query = $this->db->query('delete from agenda_trabajo where id_agenda="'.$id_agenda.'"');
  }

 public function habilitar_reserva ($id_agenda,$reserva) {
            
            $this->db->where('id_agenda',$id_agenda);
            $this->db->update('agenda_trabajo',$reserva);
          
  }
 public function getreservasdeshabilitadas()
    {
    $query = $this->db->query('select * from agenda_trabajo where estado ="1"')->result();
    return $query;
    }


}