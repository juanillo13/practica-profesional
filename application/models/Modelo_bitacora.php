<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_bitacora extends CI_Model {

  function __construct(){
    parent::__construct();
  }



public function crearBitacora($data){

   
    $this->db->insert('bitacora', $data);


}
 public function editarBitacora($id_bitacora,$dataEditarBitacora)
 {
      $this->db->where('n_bitacora',$id_bitacora);
      $this->db->update('bitacora',$dataEditarBitacora);
 }


 public function eliminar_bitacora ($n_bitacora,$bitacora) {
            
            $this->db->where('n_bitacora',$n_bitacora);
            $this->db->update('bitacora',$bitacora);
          
  }
  public function habilitar_bitacora ($n_bitacora,$bitacora) {
            
            $this->db->where('n_bitacora',$n_bitacora);
            $this->db->update('bitacora',$bitacora);
          
  }




  public function eliminar_bitacorafisica ($n_bitacora) {
            
           
           $query = $this->db->query('delete from bitacora where n_bitacora="'.$n_bitacora.'"');
  }

//BUSCA TODAS LAS BITACORAS QUE ESTEN DISPONIBLES 
  public function getbitacoras()
    {
    $query = $this->db->query('select * from bitacora where estado ="0"')->result();
    return $query;
    }


    public function getbitacorasdeshabilitadas()
    {
    $query = $this->db->query('select * from bitacora where estado ="1"')->result();
    return $query;
    }


//BUSCA UN bitacora POR SU primary key
    public function getbitacora ($n_bitacora) {
    
      $query = $this->db->query('select * from bitacora where n_bitacora="'.$n_bitacora.'"')->result();
      return $query;
  }
  public function getArchivo($n_bitacora)
  {
      $query = $this->db->query('select archivo,nombre_archivo,tipo_archivo from bitacora where n_bitacora="'.$n_bitacora.'"')->result();
      return $query;
  }

public function actividadesMensuales($patente,$mes,$anio) 
{
    $query = $this->db->query('select * from bitacora where  vehiculo_patente = "'.$patente.'" AND STR_TO_DATE(fecha, "%d/%m/%Y") BETWEEN STR_TO_DATE("01/'.$mes.'/2016", "%d/%m/%Y") AND STR_TO_DATE("31/'.$mes.'/2016", "%d/%m/%Y")')->result();

    //'" and date(fecha) between date("'.$mes.'"/01/"'.$anio.'") and date("'.$mes.'"/31/"'.$anio.'")
    return $query;


}


 

}//fin del contructor